package ai.maum.automl.api.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * mumtiple datasource 지원을 위한 STT HibernateConfig
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-21
 */
@Configuration
@ConfigurationProperties(prefix = "spring.datasource.stt")
@EnableJpaRepositories(
        entityManagerFactoryRef = "sttEntityManager",
        transactionManagerRef = "sttTransactionManager",
        basePackages = {"ai.maum.automl.**.repository.stt"}
)
public class SttHibernateConfig extends HikariConfig {

    @Bean(name = "sttDataSource")
    public DataSource sttDataSource() {
        return new LazyConnectionDataSourceProxy(new HikariDataSource(this));
    }

    @Bean(name="sttEntityManager")
    public LocalSessionFactoryBean sttEntityManagerFactory(){
        LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
        localSessionFactoryBean.setDataSource(sttDataSource());
        localSessionFactoryBean.setPackagesToScan("ai.maum.automl.**.entity.stt");
        localSessionFactoryBean.setHibernateProperties(hibernateProperties());
        localSessionFactoryBean.setPhysicalNamingStrategy(new SpringPhysicalNamingStrategy());
        return localSessionFactoryBean;
    }

    @Bean(name = "sttTransactionManager")
    public PlatformTransactionManager sttTransactionManager() {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(sttEntityManagerFactory().getObject());
        return jpaTransactionManager;
    }

    private Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
        hibernateProperties.setProperty("hibernate.physical_naming_strategy" , "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
        hibernateProperties.setProperty("hibernate.generate-ddl", "false");
        hibernateProperties.setProperty("hibernate.open-in-view", "false");
        hibernateProperties.setProperty("hibernate.show_sql", "true");
        hibernateProperties.setProperty("hibernate.generate_statistics", "false");
        hibernateProperties.setProperty("hibernate.format_sql", "true");
        hibernateProperties.setProperty("hibernate.use_sql_comments", "true");
        hibernateProperties.setProperty("hibernate.cache.use_second_level_cache", "false");
        hibernateProperties.setProperty("hibernate.cache.use_query_cache", "false");
        return hibernateProperties;
    }

}
