package ai.maum.automl.api.config;

import com.spotify.docker.client.DefaultDockerClient;
import com.spotify.docker.client.DockerClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ai.maum.automl.frontapi.config
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-15  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-15
 */
@Configuration
public class DockerConfig {

    @Bean
    DockerClient dockerClient(){
        DockerClient docker = DefaultDockerClient.builder()
                .uri("http://10.122.64.253:2376/v1.40")
                .build();
        return docker;
    }

}
