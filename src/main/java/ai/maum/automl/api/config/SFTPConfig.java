package ai.maum.automl.api.config;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 개발 환경에서 사용 될 SFTP 환경설정
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-09-15
 */
@Component
public class SFTPConfig {

    @Value("${sftp.host}")
    private String host;

    @Value("${sftp.port}")
    private int port;  //포트번호

    @Value("${sftp.user}")
    private String user;

    @Value("${sftp.password}")
    private String password;

    @Bean
    public Map<String, Object> connectSFTP() throws Exception {
        JSch jsch = new JSch();
        Session session = jsch.getSession(user, host, port);  //세션 오픈!
        session.setPassword(password);
        java.util.Properties config = new java.util.Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        session.connect();
        Channel channel = session.openChannel("sftp");
        channel.connect();
        return Map.of("session", session, "channel", channel);
    }

}
