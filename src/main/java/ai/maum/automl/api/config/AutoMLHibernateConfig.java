package ai.maum.automl.api.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Properties;

/**
 * mumtiple datasource 지원을 위한 AutoML HibernateConfig
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-21
 */
@Configuration
@ConfigurationProperties(prefix = "spring.datasource.auto-ml")
@EnableJpaRepositories(
        entityManagerFactoryRef = "autoMLEntityManager",
        transactionManagerRef = "autoMLTransactionManager",
        basePackages = {"ai.maum.automl.**.repository.automl"}
)
public class AutoMLHibernateConfig extends HikariConfig {

    @Bean(name = "autoMLDataSource")
    public DataSource autoMLDataSource() {
        return new LazyConnectionDataSourceProxy(new HikariDataSource(this));
    }

    @Primary
    @Bean(name="autoMLEntityManager")
    public LocalSessionFactoryBean autoMLEntityManagerFactory(){
        LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
        localSessionFactoryBean.setDataSource(autoMLDataSource());
        localSessionFactoryBean.setPackagesToScan("ai.maum.automl.**.entity.automl");
        localSessionFactoryBean.setHibernateProperties(hibernateProperties());
        localSessionFactoryBean.setPhysicalNamingStrategy(new SpringPhysicalNamingStrategy());
        return localSessionFactoryBean;
    }

    @Primary
    @Bean(name = "autoMLTransactionManager")
    public PlatformTransactionManager autoMLTransactionManager() {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(autoMLEntityManagerFactory().getObject());
        return jpaTransactionManager;
    }

    private Properties hibernateProperties() {
        Properties hibernateProperties = new Properties();
        hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
        hibernateProperties.setProperty("hibernate.physical_naming_strategy" , "org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy");
        hibernateProperties.setProperty("hibernate.generate-ddl", "false");
        hibernateProperties.setProperty("hibernate.open-in-view", "false");
        hibernateProperties.setProperty("hibernate.show_sql", "true");
        hibernateProperties.setProperty("hibernate.hbm2ddl.auto", "validate");
        hibernateProperties.setProperty("hibernate.generate_statistics", "false");
        hibernateProperties.setProperty("hibernate.format_sql", "true");
        hibernateProperties.setProperty("hibernate.use_sql_comments", "true");
        hibernateProperties.setProperty("hibernate.cache.use_second_level_cache", "false");
        hibernateProperties.setProperty("hibernate.cache.use_query_cache", "false");
        hibernateProperties.setProperty("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
        return hibernateProperties;
    }

}
