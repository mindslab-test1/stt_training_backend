package ai.maum.automl.api.ws;

import lombok.RequiredArgsConstructor;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * 모델 테스트 WS 컨트롤러
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-09-16
 */
@RestController
@RequiredArgsConstructor
@MessageMapping("/test")
public class ModelTestingController {

    private final SimpMessagingTemplate template;

    /**
     *
     * @param projectSeq
     * @param snapshotSeq
     */
    @MessageMapping("/{projectSeq}/{snapshotSeq}")
    public void join(@PathVariable Long projectSeq,
                     @PathVariable Long snapshotSeq) {

    }

}
