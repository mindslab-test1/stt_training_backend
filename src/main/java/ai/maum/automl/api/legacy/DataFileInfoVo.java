package ai.maum.automl.api.legacy;

import lombok.*;

import java.io.Serializable;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-09
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class DataFileInfoVo implements Serializable {
    String atchFileId;      // 파일 ID
    String atchFileDisp;    // 표시이름
    String atchFilePath;    // 파일저장경로 (상대)
    String atchFileName;    // 파일저장이름 (UUID.확장자)
    String orgFileName;     // 파일 원본 이름
    String atchFileType;    // 파일 종류 (S:음성,T:텍스트,P:이미지,V:동영상,A:패키지형태,B:바이너리), 구분자(|)로 나열
    long   atchFileSize;    // 파일 크기 (bytes)
    String smplRate;        // 샘플 레이트
    float playSec;          // 재생 시간 (초)
    float playSecAvg;       // 평균 재생 시간 (초)
    float minPlaySec;       // 최소 파일 재생 시간 (초)
    float maxPlaySec;       // 최대 파일 재생 시간 (초)
    int totFileCount;       // 총 파일 개수
    String langCd;          // 언어
    String useEosYn;        // EOS 적용 여부 (Y/N/null)
    String engnType;        // 엔진 타입
}
