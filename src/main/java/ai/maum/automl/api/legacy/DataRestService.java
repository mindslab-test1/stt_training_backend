package ai.maum.automl.api.legacy;

import ai.maum.automl.api.config.CacheKey;
import ai.maum.automl.api.model.entity.automl.project.Project;
import ai.maum.automl.api.model.entity.automl.project.ProjectSnapshot;
import ai.maum.automl.api.repository.automl.ProjectRepository;
import ai.maum.automl.api.repository.automl.ProjectSnapshotRepository;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-09
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DataRestService {

    private final ProjectRepository projectRepository;
    private final ProjectSnapshotRepository projectSnapshotRepository;

    @Value("${data.url}")
    private String dataUrl;

    private String dataApi = "/api/dmng";  // 임시 URL

    /**
     * API 관리 코드 : CD-001
     * API method : GET
     * DESC : 학습 엔진 목록 조회
     */
    @Cacheable(value = CacheKey.getEngineList, unless = "#result == null")
    public List<CodeVo> getEngineList() throws ParseException {
        String logTitle = "getEngineList/";
        String apiUrl = dataUrl + dataApi + "/getCodeList";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
                .queryParam("grpCode", "ENGN_TYPE")
                .build(false);

        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, new HttpEntity<String>(headers), String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        JSONArray dataArray = (JSONArray) jsonObject.get("data");

        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<CodeVo>>() {
        }.getType();
        List<CodeVo> result = gson.fromJson(dataArray.toString(), listType);
        log.info(logTitle + "success");
        return result;
    }

    /**
     * API 관리 코드 : CD-002
     * API method : GET
     * DESC : 언어 목록 조회
     */
    @Cacheable(value = CacheKey.getLang, unless = "#result == null")
    public List<CodeVo> getLang() throws ParseException {
        String logTitle = "getLang/";
        String apiUrl = dataUrl + dataApi + "/getCodeList";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
                .queryParam("grpCode", "LANG_CODE")
                .build(false);

        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, new HttpEntity<String>(headers), String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        JSONArray dataArray = (JSONArray) jsonObject.get("data");

        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<CodeVo>>() {
        }.getType();
        List<CodeVo> result = gson.fromJson(dataArray.toString(), listType);
        log.info(logTitle + "success");
        return result;
    }

    /**
     * API 관리 코드 : CD-101
     * API method : GET
     * DESC : STT sampling rate type 조회
     */
    @Cacheable(value = CacheKey.getSampleRateList, unless = "#result == null")
    public List<CodeVo> getSampleRateList() throws ParseException {
        String logTitle = "getSampleRateList/";
        String apiUrl = dataUrl + dataApi + "/getCodeList";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
                .queryParam("grpCode", "SMP_RATE_TYPE")
                .build(false);

        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, new HttpEntity<String>(headers), String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        JSONArray dataArray = (JSONArray) jsonObject.get("data");
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<CodeVo>>() {
        }.getType();
        List<CodeVo> result = gson.fromJson(dataArray.toString(), listType);
        log.info(logTitle + "success");
        return result;
    }

    /**
     * API 관리 코드 : DT-001
     * API method : POST
     * Desc : 사용 가능한 데이터 조회
     */
    @Cacheable(value = CacheKey.getLearningDataList, key = "{#userId, #engnType, #langCd, #useEosYn, #smplRate}", unless = "#result == null")
    public List<DataFileInfoVo> getLearningDataList(String userId, String engnType, String langCd, String useEosYn, String smplRate) throws ParseException {
        String logTitle = "getLearningDataList/userId[" + userId + "] engnType[" + engnType + "] langCd[" + langCd + "] useEosYn[" + useEosYn + "] smplRate[" + smplRate + "]/";
        String apiUrl = dataUrl + dataApi + "/getLeaningDataList";  // api url은 getLeaningDataList

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
                .queryParam("userId", userId)
                .queryParam("engnType", engnType)
                .queryParam("langCd", langCd)
                .queryParam("useEosYn", useEosYn)
                .queryParam("smplRate", smplRate)
                .build(false);

        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, new HttpEntity<String>(headers), String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        JSONArray dataArray = (JSONArray) jsonObject.get("data");

        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<DataFileInfoVo>>() {
        }.getType();
        List<DataFileInfoVo> result = gson.fromJson(dataArray.toString(), listType);
        log.info(logTitle + "success");
        return result;
    }

    /**
     * API 관리 코드 : DT-STT-001
     * API method : POST
     * DESC : 잡음 데이터 조회
     */
    @Cacheable(value = CacheKey.getNoiseDataList, key = "{#userId, #smplRate}", unless = "#result == null")
    public List<DataFileInfoVo> getNoiseDataList(String userId, String smplRate) throws ParseException {
        String logTitle = "getNoiseDataList/userId[" + userId + "] smplRate[" + smplRate + "]/";
        String apiUrl = dataUrl + dataApi + "/stt/getNoiseDataList";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
                .queryParam("userId", userId)
                .queryParam("smplRate", smplRate)
                .build(false);

        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, new HttpEntity<String>(headers), String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        JSONArray dataArray = (JSONArray) jsonObject.get("data");

        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<DataFileInfoVo>>(){}.getType();
        List<DataFileInfoVo> result = gson.fromJson(dataArray.toString(), listType);
        log.info(logTitle + "success");
        return result;
    }

    /**
     * API 관리 코드 : MT-001
     * API method : POST
     * DESC : 공통 학습 모델 조회
     */
    @Cacheable(value = CacheKey.getCommonModelList, key="{#engnType, #langCd, #smplRate, #useEosYn}", unless = "#result == null")
    public List<ModelInfoVo> getCommonModelList(String engnType, String langCd, String smplRate, String useEosYn) throws ParseException {
        String logTitle = "getCommonModelList/engnType[" + engnType + "] langCd[" + langCd + "] smplRate[" + smplRate + "] useEosYn[" + useEosYn + "]/";
        String apiUrl = dataUrl + "/api/mmng/getCommonModelList";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
                .queryParam("engnType", engnType)
                .queryParam("langCd", langCd)
                .queryParam("smplRate", smplRate)
                .queryParam("useEosYn", useEosYn)
                .build(false);

        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, new HttpEntity<String>(headers), String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        JSONArray dataArray = (JSONArray) jsonObject.get("data");

        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<ModelInfoVo>>(){}.getType();
        List<ModelInfoVo> result = gson.fromJson(dataArray.toString(), listType);
        log.info(logTitle + "success");
        return result;
    }

    /**
     * API 관리 코드 : MT-011
     * API method : POST
     * DESC : 사용자의 private 모델 조회
     */
    @Cacheable(value = CacheKey.getMyModelList, key="{#userId, #engnType, #langCd, #smplRate, #useEosYn}", unless = "#result == null")
    public List<ModelInfoVo> getMyModelList(String userId, String engnType, String langCd, String smplRate, String useEosYn) throws ParseException {
        String logTitle = "getMyModelList/userId[" + userId +"] engnType[" + engnType + "] langCd[" + langCd + "] smplRate[" + smplRate + "] useEosYn[" + useEosYn + "]/";
        String apiUrl = dataUrl + "/api/mmng/getMyModelList/";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
                // 테스트 데이터
/*                .queryParam("userId", "mhyuhyu@gmail.com")
                .queryParam("engnType", "EN_STT")*/
                .queryParam("userId", userId)
                .queryParam("engnType", engnType)
                .queryParam("langCd", langCd)
                .queryParam("smplRate", smplRate)
                .queryParam("useEosYn", useEosYn)
                .build(false);

        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, new HttpEntity<String>(headers), String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        JSONArray dataArray = (JSONArray) jsonObject.get("data");

        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<ModelInfoVo>>(){}.getType();
        List<ModelInfoVo> result = gson.fromJson(dataArray.toString(), listType);
        log.info(logTitle + "success");
        return result;
    }
    /**
     * API 관리 코드 : MT-012
     * API method : POST
     * DESC : 학습 모델 ID로 모델 데이터 리스트 조회
     */
    @Cacheable(value = CacheKey.getModelFileList, key="{#userId, #projectId}", unless = "#result == null")
    public List<ModelInfoVo> getModelFileList(Long userId, Long projectId) throws ParseException {
        String logTitle = "getModelFileList/projectId[" + projectId +"]/";
        String apiUrl = dataUrl + "/api/mmng/getModelFileList/";

        // model ID와 사용자가 일치하는지 검사 - userNo는 사용자의 정보 조회를 위해서만 사용
        Optional<Project> projectList = projectRepository.findById(projectId);

        if(projectList == null || !projectList.get().getUser().getId().equals(userId)) {
            log.error(logTitle + "failed : empty result");
            throw new EmptyResultDataAccessException(0);
        }

        String snapshotStatus = "c";
        List<ProjectSnapshot.ModelId> listModelId = projectSnapshotRepository.findByProjectIdAndStatus(projectId, snapshotStatus);

        if(listModelId == null) {
            log.error(logTitle + "failed : empty result");
            throw new EmptyResultDataAccessException(0);
        }

        List<String> mdlStr = Lists.newArrayList();
        JSONObject mdlObject = new JSONObject();

        listModelId.forEach(modelId -> {
            String str = modelId.getModelId();
            mdlStr.add(str);
        });
        mdlObject.put("mdlIds", mdlStr);

        // 테스트 데이터
/*
        List<String> newStr = Lists.newArrayList("8_STT_20201111192111645", "8_STT_20201111192111648");
        JSONObject jsonObject1 = new JSONObject();
        jsonObject1.put("mdlIds", newStr);
*/

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
                .build(false);

        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, new HttpEntity<>(mdlObject, headers), String.class);
        //ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, new HttpEntity<>(jsonObject1, headers), String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        JSONArray dataArray = (JSONArray) jsonObject.get("data");

        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<ModelInfoVo>>(){}.getType();
        List<ModelInfoVo> result = gson.fromJson(dataArray.toString(), listType);
        log.info(logTitle + "success");
        return result;
    }
}
