package ai.maum.automl.api.legacy;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-25
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ModelFileInfoVo implements Serializable {
    String mdlId;           // 모델 ID (mdl_userID_엔진타입_생성일시)
    String mdlGrp;          // 모델 그룹 명 (mdl_userID_엔진타입)
    String mdlFileName;     // 모델 데이터 파일 명 (파일명.확장자 적용)
    String mdlFilePath;     // 모델 데이터 파일 경로
    String mdlFileSize;     // 모델 데이터 파일 사이즈
    String regDate;         // 모델 데이터 파일 등록 일자
    String langCd;          // 언어
    String smplRate;        // Sampling rate
    String useEosYn;        // EOS 적용여부 (Y/N/null)
}
