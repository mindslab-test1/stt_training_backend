package ai.maum.automl.api.service;

import ai.maum.automl.api.commons.enums.AllowSampleRate;
import ai.maum.automl.api.commons.enums.ProjectStatus;
import ai.maum.automl.api.commons.utils.FileUtils;
import ai.maum.automl.api.commons.utils.LocalDateTimeSerializer;
import ai.maum.automl.api.commons.utils.ModelMapperService;
import ai.maum.automl.api.commons.websocket.ContainerAttachWebsocketUtil;
import ai.maum.automl.api.model.dto.project.ProjectDto;
import ai.maum.automl.api.model.dto.project.ProjectSnapshotDto;
import ai.maum.automl.api.model.dto.project.ProjectSnapshotDtoInterface;
import ai.maum.automl.api.model.entity.automl.project.*;
import ai.maum.automl.api.repository.automl.LearningDataRepository;
import ai.maum.automl.api.repository.automl.ModelTemplateRepository;
import ai.maum.automl.api.repository.automl.ProjectRepository;
import ai.maum.automl.api.repository.automl.ProjectSnapshotRepository;
import ai.maum.automl.api.repository.automl.UploadLearningDataRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.spotify.docker.client.exceptions.DockerException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ai.maum.automl.api.config.AsyncConfig.RUNNING_TASK;

/**
 * Created by kirk@mindslab.ai on 2020-10-21
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProjectService {

    private final ModelMapperService modelMapperService;
    private final ProjectRepository projectRepository;
    private final ProjectSnapshotRepository projectSnapshotRepository;
    private final ModelTemplateRepository modelTemplateRepository;
    private final UploadLearningDataRepository uploadLearningDataRepository;
    private final LearningDataRepository learningDataRepository;
    private final ThreadPoolTaskExecutor executor;
    private final DockerService dockerService;
    private final ContainerAttachWebsocketUtil websocketUtil;
    private final FileUtils fileUtils;

    @PersistenceContext(unitName = "autoMLEntityManager")
    EntityManager entityManager;

    private final String CONTAINER_NAME_PREFIX = "w2l_train_";

    @Transactional
    public Page<ProjectDto.ProjectDtoEx> list(Long userNo, Pageable pageable) {

//        Page<Project> list = projectRepository.findAllByUserIdOrderByIdDesc(userNo, pageable);
//        return list.map(project -> modelMapperService.map(project, ProjectDto.class));

        Page<ProjectExInterface> list2 = projectRepository.findAllByUserId(userNo, pageable);
        Page<ProjectDto.ProjectDtoEx> p2 = list2.map(project -> modelMapperService.map(project, ProjectDto.ProjectDtoEx.class));
        return p2;
    }

    @Transactional
    public void delete(Long userNo, Long id) {
        String logTitle = "delete/userNo["+ userNo +"] id[" + id + "]/";
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            log.info(logTitle + "success");
            projectRepository.deleteById(id);
        }
        else {
            log.error(logTitle + "failed: Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public ProjectSnapshotDto.Detail addSnapshot(Long userNo, Long id, String name) throws JsonProcessingException {
        String logTitle = "addSnapshot/id[" + id + "] + name[" + name + "]/";
        Optional<Project> optional  = projectRepository.findById(id);

        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            Project project = optional.get();

            String dtoString = "";
            ObjectMapper objectMapper = new ObjectMapper();
            SimpleModule simpleModule = new SimpleModule();
            simpleModule.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer());
            objectMapper.registerModule(simpleModule);

            if("STT".equalsIgnoreCase(project.getProjectType())){
                ProjectStt projectStt = (ProjectStt)project;
                ProjectDto.Stt dto = modelMapperService.map(projectStt, ProjectDto.Stt.class);
                dtoString = objectMapper.writeValueAsString(dto);
            }
            else if("TTS".equalsIgnoreCase(project.getProjectType())) {
                ProjectTts projectTts = (ProjectTts)project;
                ProjectDto.Tts dto = modelMapperService.map(projectTts, ProjectDto.Tts.class);
                dtoString = objectMapper.writeValueAsString(dto);
            }
            else if("LM".equalsIgnoreCase(project.getProjectType())) {
                ProjectLm projectTts = (ProjectLm)project;
                ProjectDto.Lm dto = modelMapperService.map(projectTts, ProjectDto.Lm.class);
                dtoString = objectMapper.writeValueAsString(dto);
            }
            else {
                return null;
            }
            log.info(logTitle + dtoString);

            ProjectSnapshot snapshot = new ProjectSnapshot();
            snapshot.setName(name);
            snapshot.setProjectId(project.getId());
            snapshot.setSnapshot(dtoString);
            snapshot.setStatus(ProjectStatus.CREATE);
            snapshot = projectSnapshotRepository.save(snapshot);
            // snapshot model id insert
            snapshot.setModelId(project.getModelName() + "_" + snapshot.getId());

            try {
                if("STT".equalsIgnoreCase(project.getProjectType())){
                    createSttTrainingContainer((ProjectStt)project, snapshot);
                }
                else if("LM".equalsIgnoreCase(project.getProjectType())) {
                    createLmTrainingContainer((ProjectLm)project, snapshot);
                }
            } catch (DockerException | InterruptedException e) {
                e.printStackTrace();
                throw new RuntimeException();
            }

            return modelMapperService.map(snapshot, ProjectSnapshotDto.Detail.class);
        }
        else {
            log.error(logTitle + "failed: Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public List<ProjectSnapshotDto.Detail> listSnapshot(Long userNo, Long id) {
        String logTitle = "listSnapshot/id[" + id + "]/";
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            List<ProjectSnapshotDtoInterface> list = projectSnapshotRepository.findAllByProjectIdOrderByRegDtDesc(id);
            log.info(logTitle + "success");
            return modelMapperService.mapAll(list, ProjectSnapshotDto.Detail.class);
        }
        else {
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public void restoreSnapshot(Long userNo, Long projectId, Long snapshotId) throws JsonProcessingException {
        String logTitle = "restoreSnapshot/id[" + projectId + "], snapshotId[" + snapshotId + "]/";
        Optional<Project> optional  = projectRepository.findById(projectId);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            Project project = optional.get();

            Optional<ProjectSnapshot> op = projectSnapshotRepository.findById(snapshotId);
            if(op.isPresent()) {
                ProjectSnapshot projectSnapshot = op.get();
                ObjectMapper objectMapper = new ObjectMapper();

                if("STT".equalsIgnoreCase(project.getProjectType())){
                    ProjectDto.Stt dto = objectMapper.readValue(projectSnapshot.getSnapshot(), ProjectDto.Stt.class);
                    ProjectStt ps = modelMapperService.map(dto, ProjectStt.class);

                    ProjectStt projectStt = (ProjectStt)project;
                    ModelTemplate modelTemplate = modelTemplateRepository.findByProjectSeqEqualsAndSnapshotSeqEquals(projectSnapshot.getProjectId(), projectSnapshot.getId());

                    // project member copy
                    projectStt.setProjectName(ps.getProjectName());
                    projectStt.setProjectDescription(ps.getProjectDescription());
                    projectStt.setProjectType(ps.getProjectType());
                    projectStt.setModelName(ps.getModelName());

                    // projectStt member copy
                    projectStt.setCompletedStep(ps.getCompletedStep());
                    projectStt.setLanguage(ps.getLanguage());
                    projectStt.setSampleRate(ps.getSampleRate());
                    projectStt.setEos(ps.getEos());
                    projectStt.setLearningDataList(ps.getLearningDataList());
                    projectStt.setEpochSize(ps.getEpochSize());
                    projectStt.setBatchSize(ps.getBatchSize());
                    projectStt.setRate(ps.getRate());
                    projectStt.setSampleLength(ps.getSampleLength());
                    projectStt.setMinNoiseSample(ps.getMinNoiseSample());
                    projectStt.setMaxNoiseSample(ps.getMaxNoiseSample());
                    projectStt.setMinSnr(ps.getMinSnr());
                    projectStt.setMaxSnr(ps.getMaxSnr());
                    projectStt.setNoiseDataList(ps.getNoiseDataList());
                    projectStt.setPreTrainedModel(
                            PreTrainedModel.builder()
                                    .modelTemplateId(modelTemplate.getModelId())
                                    .mdlId(modelTemplate.getModelName()+"-"+projectSnapshot.getName())
                                    .mdlFileName(modelTemplate.getModelFileName())
                                    .mdlFilePath(modelTemplate.getModelPath())
                                    .build());
                    projectRepository.save(projectStt);
                    log.info(logTitle + "success - STT");
                    return;
                }
                else if("TTS".equalsIgnoreCase(project.getProjectType())){
                    ProjectDto.Tts dto = objectMapper.readValue(projectSnapshot.getSnapshot(), ProjectDto.Tts.class);
                    project = modelMapperService.map(dto, ProjectTts.class);
                    projectRepository.save(project);
                    log.info(logTitle + "success - TTS");
                    return;
                }
            }
        }
        log.error(logTitle + "failed : Empty result");
        throw new EmptyResultDataAccessException(0);
    }

    public ProjectDto get(Long userNo, Long id) {
        String logTitle = "get/userNo[" + userNo + "] id[" + id + "]/";
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            log.info(logTitle + "success");
            return modelMapperService.map(optional.get(), ProjectDto.class);
        }
        else {
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }


    /**
     * 엔진타입, 언어 코드, 샘플링레이트 코드와 유저 SEQ로 모델 템플릿 조회
     * @param id 회원 ID (고유번호), NULL일 경우 사전학습모델
     * @param engnType 엔진 타입
     * @param langCd 언어 코드
     * @param smplRate 샘플링레이트 코드
     * @return 모델 템플릿 목록
     */
    public List<ModelTemplate> getModelTemplates(Long id, String engnType, String langCd, String smplRate){
        return modelTemplateRepository.findByUserSeqEqualsAndEngnTypeEqualsAndLangCdEqualsAndSmplRateEquals(id, engnType, langCd, smplRate);
    }

    /**
     * 회원ID, 프로젝트ID, 엔진 타입, 언어 코드, 샘플링 레이트 코드로 학습모델 TREE 가공
     * @param userSeq 회원 ID (고유번호)
     * @param projectId 프로젝트 ID
     * @param engnType 엔진 타입
     * @param langCd 언어 코드
     * @param smplRate 샘플링레이트 코드
     * @return 가공된 학습모델 TREE
     */
    public List<ModelTemplate> getProjectModelTemplates(Long userSeq, Long projectId, String engnType, String langCd, String smplRate){
        if("LM".equals(engnType)){
            return modelTemplateRepository.findByUserSeqEqualsAndProjectSeqEqualsAndEngnTypeEqualsAndLangCdEquals(userSeq, projectId, engnType, langCd);
        }else{
            return modelTemplateRepository.findByUserSeqEqualsAndProjectSeqEqualsAndEngnTypeEqualsAndLangCdEqualsAndSmplRateEquals(userSeq, projectId, engnType, langCd, smplRate);
        }
    }

    /**
     * 회원ID, 엔진 타입, 언어 코드로 학습모델 TREE 가공
     * @param userSeq 회원 ID (고유번호)
     * @param engnType 엔진타입
     * @param langCd 언어 코드
     * @return 가공된 학습모델 TREE
     */
    public List<ModelTemplate> getTargetModelTemplates(Long userSeq, String engnType, String langCd){
        if(!"LM".equals(engnType)){
            return Stream.of(
                    modelTemplateRepository.findByUserSeqEqualsAndEngnTypeEqualsAndLangCdEquals(userSeq, engnType, langCd),
                    modelTemplateRepository.findByUserSeqIsNullAndEngnTypeEqualsAndLangCdEquals(engnType, langCd)
            ).flatMap(Collection::stream).collect(Collectors.toList());
        }else{
            return modelTemplateRepository.findByUserSeqEqualsAndEngnTypeEqualsAndLangCdEquals(userSeq, engnType, langCd);
        }
    }


    /**
     * 프로젝트 정보를 학습 executor queue에 적재
     * @param project 프로젝트 스냅샷
     */
    public void addTrainingExecutorQueue (ProjectSnapshot project, String projectType){
        RUNNING_TASK.put(CONTAINER_NAME_PREFIX+project.getProjectId()+"_"+project.getId(), executor.submit(() -> {
            try {
                boolean dockerRunnable = true;
                websocketUtil.attachContainer(CONTAINER_NAME_PREFIX+project.getProjectId()+"_"+project.getId(), project, null, projectType);
                while (dockerRunnable) {
                    dockerRunnable = dockerService.checkContainerStartCount();
                    if(dockerRunnable){
                        // 5sec delay
                        Thread.sleep(5000L);
                    }
                }
                if(projectSnapshotRepository.updProjectSnapshotStatus(entityManager, project.getId(), ProjectStatus.START)) {
                    dockerService.startContainer(CONTAINER_NAME_PREFIX+project.getProjectId()+"_"+project.getId());
                } else {
                    log.error("project snapshot status update fail");
                    throw new RuntimeException();
                }
            } catch (DockerException | InterruptedException e) {
                log.error("training executor error");
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }));
    }

    /**
     * STT 학습 컨테이너 생성
     * @param project stt
     * @param project snapshot
     * @throws DockerException
     * @throws InterruptedException
     */
    private void createSttTrainingContainer(ProjectStt project, ProjectSnapshot projectSnapshot) throws DockerException, InterruptedException {
        /*docker run -it --gpus all
            -e NVIDIA_VISIBLE_DEVICES=0
            -e LC_ALL=C.UTF-8
            -e DATA_LIST=ATCH:/DATA/snt
            -e W2L_LANG=kor
            -e SAMPLERATE=8000
            -e MAX_DURATION=600000
            -e VALIDATION_RATE=0.1
            -e RESULT_DIR=/MODEL
            -e USE_EOS=true
            -e EPOCH=1
            -e BATCH_SIZE=1
            -e MIN_NOISE_SAMPLE=200
            -e MAX_NOISE_SAMPLE=400
            -e MIN_SNR=5
            -e MAX_SNR=20
            -e NOISE_LIST=/NOISE/voicebot,/NOISE/musan
            -e BASELINE=/BASELINE/001_model_dev.bin
            --name w2l_train01
            -v "/DATA/AutoML_DATA/tmp/nvidia-mps:/tmp/nvidia-mps"
            -v "/DATA/AutoML_DATA/Train:/DATA/snt"
            -v "/DATA/AutoML_DATA/voicebot/data:/NOISE/voicebot"
            -v "/DATA/AutoML_DATA/musan:/NOISE/musan"
            -v "/DATA/AutoML_DATA/train_test:/MODEL"
            -v "/DATA/AutoML_DATA/model:/BASELINE"
            docker.maum.ai:443/brain/w2l:1.1.0-train*/
        dockerService.createContainer("w2l:1.1.0-train-edit",
                "w2l_train_"+project.getId()+"_"+projectSnapshot.getId(),
                Arrays.asList(
                        "NVIDIA_VISIBLE_DEVICES=0", // 고정값
                        "LC_ALL=C.UTF-8", // 고정값
                        "DATA_LIST=ATCH:/DATA/training", // DATALIST에는 명칭을 줄 수 있음.
                        "W2L_LANG="+(project.getLanguage().equalsIgnoreCase("LN_KO") ? "kor" : "eng"),
                        "SAMPLERATE="+ AllowSampleRate.valueOf(project.getSampleRate()).getValue(),
                        "MAX_DURATION="+(project.getSampleLength()*1000), // sec
                        "VALIDATION_RATE="+project.getRate(),
                        "RESULT_DIR=/MODEL",
                        "USE_EOS=true", // 설정가능
                        "EPOCH="+project.getEpochSize(), // 설정가능
                        "BATCH_SIZE="+project.getBatchSize(), // 설정가능
                        "MIN_NOISE_SAMPLE="+project.getMinNoiseSample(), // 설정가능
                        "MAX_NOISE_SAMPLE="+project.getMaxNoiseSample(), // 설정가능
                        "MIN_SNR="+project.getMinSnr(), // 설정가능
                        "MAX_SNR="+project.getMaxSnr(), // 설정가능
                        "NOISE_LIST="+project.getNoiseDataList().stream().map(v -> "/NOISE/"+v.getAtchFileId()).collect(Collectors.joining(",")), // 변동값 (아래 NOISE 볼륨과 연계)
                        "BASELINE=/BASELINE/"+project.getPreTrainedModel().getMdlFileName() // 변동값 (아래 BASELINE 볼륨과 연계)
                ),
                Stream.of(
                        Arrays.asList(
                                "/DATA/AutoML_DATA/tmp/nvidia-mps:/tmp/nvidia-mps",
                                "/DATA/AutoML_DATA/"+project.getId()+"/train"+":/DATA/training",
                                "/DATA/AutoML_DATA/"+project.getId()+"/"+projectSnapshot.getId()+"/result:/MODEL",
                                "/DATA:/HOST"
                        ),
                        project.getNoiseDataList().stream().map(v -> v.getAtchFilePath()+":/NOISE/"+v.getAtchFileId()).collect(Collectors.toList()),
                        Arrays.asList(project.getPreTrainedModel().getMdlFilePath()+":/BASELINE")
                ).flatMap(Collection::stream).collect(Collectors.toList())
                , true
        );
    }

    /**
     * 파일삭제 (단건)
     * @param uploadSeq 파일순번
     */
    @Transactional
    public void deleteFile(Long uploadSeq){
        List<UploadLearningData> list = uploadLearningDataRepository.findUploadLearningDataById(entityManager, uploadSeq);
        fileUtils.deleteFile(list);
        list.forEach(v -> learningDataRepository.deleteLearningDataByAtchFileIdEquals(v.getUploadSeq().toString()));
        uploadLearningDataRepository.deleteAll(list);
    }

    @Transactional
    public void deleteNotUsedFiles(){
        fileUtils.deleteFile(uploadLearningDataRepository.findUploadLearningDataByIsUsedEquals(false));
        uploadLearningDataRepository.deleteByIsUsed(entityManager, false);
    }

    /**
     * LM 학습 컨테이너 생성
     * @param project lm
     * @param project snapshot
     * @throws DockerException
     * @throws InterruptedException
     */
    private void createLmTrainingContainer(ProjectLm project, ProjectSnapshot projectSnapshot) throws DockerException, InterruptedException {
        /*w2l_train_lm:
        image: docker.maum.ai:443/brain/w2l:1.2.5-train-lm
        environment:
        - "LC_ALL=C.UTF-8"
                - "W2L_LANG=kor"
                - "USE_EOS=true"
                - "USE_TKN_LM=true"
                - "DATA_ENCODING=utf-8"
                - "DATA_DIR=/DATA"
                - "RESULT_DIR=/MODEL"
        ipc: host
        volumes:
        - "/data/lm_test/data:/DATA"
                - "/data/lm_test/model:/MODEL"*/
        dockerService.createContainer("docker.maum.ai:443/brain/w2l:1.2.5-train-lm",
                "w2l_train_"+project.getId()+"_"+projectSnapshot.getId(),
                Arrays.asList(
                        "DATA_DIR=/DATA/training", // DATALIST에는 명칭을 줄 수 있음.
                        "RESULT_DIR=/MODEL", // 결과물이 떨어지는 디렉토리 / volume으로 연결되어있음
                        "W2L_LANG="+(project.getLanguage().equalsIgnoreCase("LN_KO") ? "kor" : "eng"), // kor/eng... eng하니까 에러나던데 이건 어떻게?
                        "USE_EOS=true",
                        "USE_TKN_LM=true",
                        "DATA_ENCODING=utf-8",
                        "LC_ALL=C.UTF-8"
                ),
                Arrays.asList(
                        "/DATA/AutoML_DATA/"+project.getId()+"/train_lm"+":/DATA/training",
                        "/DATA/AutoML_DATA/"+project.getId()+"/"+projectSnapshot.getId()+"/result:/MODEL"
                )
                , false
        );
    }

}
