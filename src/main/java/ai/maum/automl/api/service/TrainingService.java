package ai.maum.automl.api.service;

import ai.maum.automl.api.model.entity.automl.project.Project;
import ai.maum.automl.api.model.entity.automl.project.ProjectLm;
import ai.maum.automl.api.model.entity.automl.project.ProjectSnapshot;
import ai.maum.automl.api.model.entity.automl.project.ProjectStt;
import ai.maum.automl.api.repository.automl.ProjectRepository;
import ai.maum.automl.api.repository.automl.ProjectSnapshotRepository;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Future;

import static ai.maum.automl.api.config.AsyncConfig.RUNNING_TASK;


@Slf4j
@Service
@RequiredArgsConstructor
public class TrainingService {

    @Value("${backend.api.url}")
    private String backendApiUrl;

    private final String trainApiPath = "/api/training";

    private final ProjectSnapshotRepository projectSnapshotRepository;
    private final ProjectRepository projectRepository;
    private final ProjectService projectService;
    private final DockerService dockerService;

    private final String CONTAINER_NAME_PREFIX = "w2l_train_";

    /**
     * snapshot이 로그인한 사용자의 것인지 조회
     **/
    private Optional<Project> getOptionalProject(Long snapshotId){
        Optional<ProjectSnapshot> optionalProjectSnapshot = projectSnapshotRepository.findById(snapshotId);
        Long projectId = optionalProjectSnapshot.get().getProjectId();
        Optional<Project> optionalProject = projectRepository.findById(projectId);

        return optionalProject;
    }

    /**
     * API method : POST
     * DESC : 학습 시작
    **/
    public Map<String, Object> beginTraining(Long userNo, Long snapshotId, String trainingId) throws ParseException {
        String logTitle = "beginTraining/snapshotId[" + snapshotId + "], trainingId[" + trainingId + "]/";
        Optional<Project> optionalProject = getOptionalProject(snapshotId);
        Map<String, Object> resMap = null;
        // snapshot이 로그인한 사용자의 것인지 조회
        if(optionalProject.isPresent() && optionalProject.get().getUser().getId().equals(userNo)) {
            // 학습 요청 후 completedStep 업데이트
            if("STT".equals(optionalProject.get().getProjectType())){
                ProjectStt project = (ProjectStt)optionalProject.get();
                if(project.getCompletedStep() < 2){
                    project.setCompletedStep(2);
                    projectRepository.save(project);
                    log.info(logTitle + "update project step to 2");
                }
                try {
                    projectService.addTrainingExecutorQueue(projectSnapshotRepository.findById(snapshotId).get(), project.getProjectType());
                    resMap = Map.of("success", true, "message", "학습 요청 성공");
                } catch (Exception e) {
                    resMap = Map.of("success", false, "message", "학습 요청 실패", "errorCode", e.getMessage());
                }
            }else if("LM".equals(optionalProject.get().getProjectType())){
                ProjectLm project = (ProjectLm)optionalProject.get();
                if(project.getCompletedStep() < 2){
                    project.setCompletedStep(2);
                    projectRepository.save(project);
                    log.info(logTitle + "update project step to 2");
                }
                try {
                    projectService.addTrainingExecutorQueue(projectSnapshotRepository.findById(snapshotId).get(), project.getProjectType());
                    resMap = Map.of("success", true, "message", "학습 요청 성공");
                } catch (Exception e) {
                    resMap = Map.of("success", false, "message", "학습 요청 실패", "errorCode", e.getMessage());
                }
            }
        }else{
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }
        return resMap;
    }

    /**
     * API method : PUT
     * DESC : 학습 종료(학습중인 경우)
     **/
    public Map<String, Object> finishTraining(Long userNo, Long snapshotId, String trainingId) throws ParseException {
        String logTitle = "finishTraining/snapshotId[" + snapshotId + "], trainingId[" + trainingId + "]/";
        Optional<Project> optionalProject = getOptionalProject(snapshotId);

        // snapshot이 로그인한 사용자의 것인지 조회
        if(optionalProject.isPresent() && optionalProject.get().getUser().getId().equals(userNo)) {
            /*String apiUrl = backendApiUrl + trainApiPath + "/finish";

            JSONObject reqObj = new JSONObject();
            reqObj.put("snapshotId", snapshotId);
            reqObj.put("trainingId", trainingId);

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(new MediaType("application", "json", StandardCharsets.UTF_8));
            HttpEntity<String> entity = new HttpEntity<>(reqObj.toString(), headers);

            ResponseEntity<String> response = restTemplate.exchange(apiUrl, HttpMethod.PUT, entity, String.class);

            int responseCode = response.getStatusCode().value();
            if(responseCode != 200) {
                log.error(logTitle + "failed : responseCode=" + responseCode);
                throw new RuntimeException();
            }

            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
            Map<String, Object> responseBody = new Gson().fromJson(jsonObject.toString(), HashMap.class);
            log.info(logTitle + "success");


            Optional<ProjectSnapshot> optionalProjectSnapshot = projectSnapshotRepository.findById(snapshotId);
            String status = optionalProjectSnapshot.get().getStatus().name();

            if("false".equalsIgnoreCase(jsonObject.get("success").toString())) {    // api error
                responseBody.put("success", jsonObject.get("success"));
                responseBody.put("message", jsonObject.get("message"));
                responseBody.put("errorCode", jsonObject.get("errorCode"));
            }
            else if(status.equals("e")) {
                responseBody.put("success", "fail");
                responseBody.put("message", "Training status : e");
                responseBody.put("errorCode", "");
            }
            else {
                // 학습 종료 후 completedStep 업데이트
                ProjectStt project = (ProjectStt)optionalProject.get();
                if(project.getCompletedStep() < 3){
                    project.setCompletedStep(3);
                    projectRepository.save(project);
                    log.info(logTitle + "update project step to 3");
                }

                responseBody.put("success", jsonObject.get("success"));
                responseBody.put("message", jsonObject.get("message"));
            }*/
            /*RUNNING_TASK.get(snapshotId).cancel(true);*/
            // FIXME 학습종료, 다방면에서 검증 필요
            String containerName = CONTAINER_NAME_PREFIX+optionalProject.get().getId()+"_"+snapshotId;
            if(dockerService.getContainerInfo(containerName).state().running()){
                dockerService.stopContainer(containerName);
            }
            ProjectStt project = (ProjectStt)optionalProject.get();
            if(project.getCompletedStep() < 3){
                project.setCompletedStep(3);
                projectRepository.save(project);
                log.info(logTitle + "update project step to 3");
            }
            Map<String, Object> responseBody = new HashMap<>();
            responseBody.put("success", "success");
            responseBody.put("message", "message");
            return responseBody;
        }else{
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }


    /**
     * API method : PUT
     * DESC : 학습 취소(대기중인 경우)
     **/
    public Map<String, Object> cancelTraining(Long userNo, Long snapshotId, String trainingId) throws ParseException {
        String logTitle = "cancelTraining/snapshotId[" + snapshotId + "], trainingId[" + trainingId + "]/";
        Optional<Project> optionalProject = getOptionalProject(snapshotId);
        // snapshot이 로그인한 사용자의 것인지 조회
        if(optionalProject.isPresent() && optionalProject.get().getUser().getId().equals(userNo)) {
            // FIXME 학습취소, 다방면에서 검증 필요
            Map<String, Object> responseBody = new HashMap<>();
            String key = CONTAINER_NAME_PREFIX+optionalProject.get().getId()+"_"+snapshotId;
            Future task = RUNNING_TASK.get(key);
            if(Optional.ofNullable(task).isPresent()){
                task.cancel(true);
                RUNNING_TASK.remove(key);
            }
            // snapshot delete
            projectSnapshotRepository.deleteProjectSnapshotById(snapshotId);
            // 학습 요청 후 completedStep 업데이트
            if("STT".equals(optionalProject.get().getProjectType())){
                ProjectStt project = (ProjectStt)optionalProject.get();
                if(project.getCompletedStep() == 2){
                    project.setCompletedStep(1);
                    projectRepository.save(project);
                    log.info(logTitle + "update project step to 1");
                }
            }else if("LM".equals(optionalProject.get().getProjectType())){
                ProjectLm project = (ProjectLm)optionalProject.get();
                if(project.getCompletedStep() == 2){
                    project.setCompletedStep(1);
                    projectRepository.save(project);
                    log.info(logTitle + "update project step to 1");
                }
            }
            return Map.of("success", "success", "message", "학습 취소 성공");
        }else{
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }
}
