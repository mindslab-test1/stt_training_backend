package ai.maum.automl.api.service;

import com.spotify.docker.client.DockerClient;
import com.spotify.docker.client.LogStream;
import com.spotify.docker.client.exceptions.DockerException;
import com.spotify.docker.client.messages.*;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * ai.maum.automl.frontapi.service
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-14  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-14
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class DockerService {

    DockerClient docker;

    @Autowired
    public DockerService(DockerClient dockerClient) {
        this.docker = dockerClient;
    }

    /**
     * 컨테이너 사용가능 여부 확인
     * @return
     * @throws DockerException
     * @throws InterruptedException
     */
    public boolean checkContainerStartCount() throws DockerException, InterruptedException {
        List<Container> list = docker.listContainers(DockerClient.ListContainersParam.allContainers());
        return list.stream()
                .filter(v -> "running".equalsIgnoreCase(v.state())).anyMatch(v -> v.names().stream().anyMatch(sv -> sv.contains("w2l_")));
    }

    /**
     * 유휴 컨테이너 확인
     * @return
     * @throws DockerException
     * @throws InterruptedException
     */
    public List<Container> getIdleContainers() throws DockerException, InterruptedException {
         return docker.listContainers(DockerClient.ListContainersParam.withStatusExited()).stream().filter(v -> v.names().stream().anyMatch(sv -> sv.contains("w2l_"))).collect(Collectors.toList());
    }

    /**
     * 컨테이너 삭제
     * @param id
     * @throws DockerException
     * @throws InterruptedException
     */
    public void deleteContainer(String id) throws DockerException, InterruptedException {
        docker.removeContainer(id);
    }

    /**
     * 컨테이너 정보 확인
     * @param name 컨테이너 이름
     * @return 컨테이너 정보
     * @throws DockerException
     * @throws InterruptedException
     */
    public ContainerInfo getContainerInfo(String name) {
        try {
            return docker.inspectContainer(name);
        } catch (DockerException | InterruptedException e) {
            return null;
        }
    }

    /**
     * get container state exit code
     * @param name container name
     * @return exitCode
     * @throws DockerException
     * @throws InterruptedException
     */
    public Long getContainerStateExitCode(String name) throws DockerException, InterruptedException {
        return docker.inspectContainer(name)
                .state()
                .exitCode();
    }

    /**
     * stop container
     * @param name container name
     */
    public void stopContainer(String name){
        try {
            docker.stopContainer(name, 0);
        } catch (DockerException | InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * 학습/테스트 컨테이너 생성
     * @param image 사용 이미지
     * @param name 컨테이너 이름
     * @param envs 컨테이너 환경변수
     * @param volumes 컨테이너 볼륨매핑 정보
     * @return 컨테이너 ID
     * @throws DockerException
     * @throws InterruptedException
     */
    public String createContainer(String image, String name, List<String> envs, List<String> volumes, boolean useGpu) throws DockerException, InterruptedException {
        ContainerConfig containerConfig = ContainerConfig.builder()
                .image(image)
                .env(envs)
                .hostConfig(HostConfig.builder()
                        .runtime(useGpu ? "nvidia" : null)
                        .binds(volumes)
                        .build())
                .build();
        ContainerCreation container = docker.createContainer(containerConfig, name);
        return container.id();
    }

    /**
     * start container
     * @param name container name
     * @throws DockerException
     * @throws InterruptedException
     */
    public void startContainer(String name) throws DockerException, InterruptedException {
        docker.startContainer(name);
    }

    /**
     * exec
     * @param name container name
     * @param args args
     * @throws DockerException
     * @throws InterruptedException
     */
    public void exec(String name, List<String> args) throws DockerException, InterruptedException {
        docker.execStart(docker.execCreate(name, args.toArray(new String[args.size()])).id());
    }

    /**
     * log
     * @param name container name
     * @param logsParams params
     * @return
     * @throws DockerException
     * @throws InterruptedException
     */
    public LogStream logs(String name, DockerClient.LogsParam... logsParams) throws DockerException, InterruptedException {
        return docker.logs(name, logsParams);
    }

}
