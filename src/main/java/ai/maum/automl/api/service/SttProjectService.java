package ai.maum.automl.api.service;

import ai.maum.automl.api.commons.enums.AllowFileExt;
import ai.maum.automl.api.commons.enums.AllowSampleRate;
import ai.maum.automl.api.commons.exception.FileNotAllowedException;
import ai.maum.automl.api.commons.utils.FileUtils;
import ai.maum.automl.api.commons.utils.ModelMapperService;
import ai.maum.automl.api.model.dto.project.ProjectDto;
import ai.maum.automl.api.model.dto.project.request.LearningDataUploadRequest;
import ai.maum.automl.api.model.dto.project.response.LearningDataUploadResponse;
import ai.maum.automl.api.model.entity.automl.common.User;
import ai.maum.automl.api.model.entity.automl.project.*;
import ai.maum.automl.api.repository.automl.NoiseDataTemplateRepository;
import ai.maum.automl.api.repository.automl.ProjectRepository;
import ai.maum.automl.api.repository.automl.UploadLearningDataRepository;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.Tika;
import org.jaudiotagger.audio.AudioHeader;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by kirk@mindslab.ai on 2020-10-20
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SttProjectService {

    @Value("${ml.data.train}")
    private String trainPath;

    @Value("${ml.data.root}")
    private String rootPath;

    private final ModelMapperService modelMapperService;
    private final ProjectRepository projectRepository;
    private final UploadLearningDataRepository uploadLearningDataRepository;
    private final NoiseDataTemplateRepository noiseDataTemplateRepository;
    private final FileUtils fileUtils;

    @PersistenceContext(unitName = "autoMLEntityManager")
    EntityManager entityManager;

    @Transactional
    public ProjectDto.Stt add(Long userNo, ProjectDto dto) {
        if("STT".equalsIgnoreCase(dto.getProjectType())) {
            User user = new User(userNo);
            ProjectStt projectStt = modelMapperService.map(dto, ProjectStt.class);
            projectStt.setCompletedStep(0);
            projectStt.setUser(user);

            String modelName = userNo.toString() + "_STT_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));

            projectStt.setModelName(modelName);
            projectStt = projectRepository.save(projectStt);

            return modelMapperService.map(projectStt, ProjectDto.Stt.class);
        } else {
            log.error("not supported projectType:" + dto.getProjectType());
            return null;
        }
    }

    @Transactional
    public ProjectDto.Stt get(Long userNo, Long id) {
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            Project project = optional.get();
            if("STT".equalsIgnoreCase(project.getProjectType())){
                ProjectStt projectStt = (ProjectStt)project;
                ProjectDto.Stt dto = modelMapperService.map(projectStt, ProjectDto.Stt.class);
                if(Optional.ofNullable(projectStt.getPreTrainedModel()).isPresent()){
                    dto.setPreTrainedModel(
                            ModelTemplate.builder()
                                    .modelId(projectStt.getPreTrainedModel().getModelTemplateId())
                                    .modelName(projectStt.getPreTrainedModel().getMdlFileName())
                                    .modelFileName(projectStt.getPreTrainedModel().getMdlFileName())
                                    .modelPath(projectStt.getPreTrainedModel().getMdlFilePath())
                                    .build()
                    );
                }
                return dto;
            }
            else {
                return null;
            }
        }
        else {
            throw new EmptyResultDataAccessException(0);
        }
    }
    
    @Transactional
    public void updateStep(Long userNo, Long id, int step){
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            ProjectStt project = (ProjectStt)optional.get();
            project.setCompletedStep(step);
            projectRepository.save(project);
        } else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public void learningData(Long userNo, Long id, ProjectDto.LearningDataInput dto) {
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            ProjectStt project = (ProjectStt)optional.get();
            project.setLanguage(dto.getLanguage());
            project.setSampleRate(dto.getSampleRate());
            project.setEos(dto.getEos());
            project.setLearningDataList(
                    dto.getLearningDataList().stream()
                    .map(v -> {
                        UploadLearningData data = uploadLearningDataRepository.findById(Long.parseLong(v.getAtchFileId()))
                                .orElseThrow(() -> {
                                    throw new EmptyResultDataAccessException(0);
                                });
                        v.setAtchFileName(data.getAtchFilePairName());
                        v.setAtchFileDisp(data.getAtchFilePairName());
                        v.setLangCd(dto.getLanguage());
                        v.setTotFileCount(2);
                        v.setAtchFilePath(rootPath+trainPath+"/"+id);
                        return v;
                    }).collect(Collectors.toList())
            );
            //TODO: step enum 만들것.
            if(project.getCompletedStep() < 1)
                project.setCompletedStep(1);
            projectRepository.save(project);
            // 미사용 파일 삭제
            // TODO : 배치 작성하여 주기적으로 미사용 파일 삭제해야함.
            uploadLearningDataRepository.updateIsUsedByUpdateSeqIn(entityManager, dto.getLearningDataList().stream().map(v -> {
                List<UploadLearningData> list = uploadLearningDataRepository.findUploadLearningDataById(entityManager, Long.parseLong(v.getAtchFileId()));
                return list;
            }).flatMap(Collection::stream).map(UploadLearningData::getUploadSeq).collect(Collectors.toList()));
            uploadLearningDataRepository.deleteByIsUsed(entityManager, false);
        } else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public void trainingData(Long userNo, Long id, ProjectDto.TrainingDataInput dto) {
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            ProjectStt project = (ProjectStt)optional.get();
            project.setEpochSize(dto.getEpochSize());
            project.setBatchSize(dto.getBatchSize());
            project.setRate(dto.getRate());
            project.setSampleLength(dto.getSampleLength());
            project.setNoiseDataList(dto.getNoiseDataList());
            project.setMinNoiseSample(dto.getMinNoiseSample());
            project.setMaxNoiseSample(dto.getMaxNoiseSample());
            project.setMinSnr(dto.getMinSnr());
            project.setMaxSnr(dto.getMaxSnr());
            project.setCheckSave(dto.getCheckSave());
            project.setPreTrainedModel(
                    PreTrainedModel.builder()
                            .mdlId(dto.getPreTrainedModel().getModelName())
                            .modelTemplateId(dto.getPreTrainedModel().getModelId())
                            .mdlFilePath(dto.getPreTrainedModel().getModelPath())
                            .mdlFileName(dto.getPreTrainedModel().getModelFileName())
                    .build()
            );
            //TODO: step enum 만들것.
//            if(project.getCompletedStep() < 2)
//                project.setCompletedStep(2);

            projectRepository.save(project);
        }
        else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    /**
     * 학습 데이터 업로드
     * @param id 프로젝트 ID
     * @param userNo 회원 SEQ
     * @param learningDataUploadRequest 업로드 요청
     * @return
     * @throws FileNotAllowedException
     */
    @Transactional
    public List<LearningDataUploadResponse> learningDataUpload(Long id, Long userNo, LearningDataUploadRequest learningDataUploadRequest) {
        String logTitle = "learningDataUpload/id[" + id + "]/";
        Optional<Project> optional  = projectRepository.findById(id);

        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            if(learningDataUploadRequest.getFiles().stream().allMatch(v -> {
                try {
                    return AllowFileExt.findByMimeType(new Tika().detect(v.getInputStream())) && Optional.ofNullable(AllowFileExt.findByValue(v.getOriginalFilename().split("\\.")[1])).isPresent();
                } catch (IOException e) {
                    throw new RuntimeException(e.getMessage());
                }
            })){
                Project project = optional.get();
                Map<String, List<MultipartFile>> fileGroup = learningDataUploadRequest.getFiles().stream().collect(Collectors.groupingBy(v -> v.getOriginalFilename().split("\\.")[0]));
                return uploadLearningDataRepository.saveAll(fileGroup.entrySet()
                        .stream()
                        .map(v -> {
                            String uuid = UUID.randomUUID().toString();
                            return v.getValue().stream().map(sv -> {
                                if(AllowFileExt.WAV.equals(AllowFileExt.findByValue(sv.getOriginalFilename().split("\\.")[1]))){
                                    try {
                                        return fileUtils.createAudioFile(uuid, id, sv, rootPath, trainPath);
                                    } catch (IOException | CannotReadException | TagException | InvalidAudioFrameException | ReadOnlyFileException e) {
                                        throw Throwables.propagate(e);
                                    }
                                } else {
                                    try {
                                        return fileUtils.createTxtFile(uuid, id, sv, learningDataUploadRequest.getLang(), rootPath, trainPath);
                                    } catch (IOException e) {
                                        throw Throwables.propagate(e);
                                    }
                                }
                            }).collect(Collectors.toList());
                        }).flatMap(Collection::stream).distinct()
                        .map(v -> UploadLearningData.builder()
                                .project(project)
                                .atchFilePath(rootPath+"/"+id+trainPath+"/"+((File)v.get("file")).getName())
                                .atchFileName(((File)v.get("file")).getName())
                                .atchFileSize(((MultipartFile)v.get("multipart")).getSize())
                                .atchFileExt(AllowFileExt.findByValue(((MultipartFile)v.get("multipart")).getOriginalFilename().split("\\.")[1]))
                                .atchFileOrgName(((MultipartFile)v.get("multipart")).getOriginalFilename())
                                .atchFilePairName(((MultipartFile)v.get("multipart")).getOriginalFilename().split("\\.")[0])
                                .regDt(LocalDateTime.now())
                                .audioInfo((AudioHeader)v.get("audio"))
                                .build()).collect(Collectors.toSet()))
                        .stream()
                        .filter(v -> AllowFileExt.WAV.equals(v.getAtchFileExt()))
                        .map(v -> LearningDataUploadResponse.builder()
                                .atchFileId(v.getUploadSeq())
                                .projectId(id)
                                .atchFileDisp(v.getAtchFilePairName())
                                .atchFilePairName(v.getAtchFilePairName())
                                .playSec(v.getAudioInfo() != null ? v.getAudioInfo().getTrackLength() : 0)
                                .playSecAvg(v.getAudioInfo() != null ? v.getAudioInfo().getTrackLength() : 0)
                                .smplRate(v.getAudioInfo() != null ? AllowSampleRate.findByValue(Integer.parseInt(v.getAudioInfo().getSampleRate())).toString() : null)
                                .regDt(v.getRegDt().format(DateTimeFormatter.ISO_DATE_TIME))
                                .atchFileType("S") // STT 고정
                                .build())
                        .collect(Collectors.toList());
                /*return uploadLearningDataRepository.saveAll(learningDataUploadRequest.getFiles().stream()
                                .map(v -> {
                                    if(AllowFileExt.WAV.equals(AllowFileExt.findByValue(v.getOriginalFilename().split("\\.")[1]))){
                                        try {
                                            return fileUtils.createAudioFile(id, v, rootPath, trainPath);
                                        } catch (IOException | CannotReadException | TagException | InvalidAudioFrameException | ReadOnlyFileException e) {
                                            throw Throwables.propagate(e);
                                        }
                                    } else {
                                        try {
                                            return fileUtils.createTxtFile(id, v, learningDataUploadRequest.getLang(), rootPath, trainPath);
                                        } catch (IOException e) {
                                            throw Throwables.propagate(e);
                                        }
                                    }
                                }).map(v -> UploadLearningData.builder()
                                        .project(project)
                                        .atchFilePath(((File)v.get("file")).getAbsolutePath())
                                        .atchFileName(((File)v.get("file")).getName())
                                        .atchFileSize(((MultipartFile)v.get("multipart")).getSize())
                                        .atchFileExt(AllowFileExt.findByValue(((MultipartFile)v.get("multipart")).getOriginalFilename().split("\\.")[1]))
                                        .atchFileOrgName(((MultipartFile)v.get("multipart")).getOriginalFilename())
                                        .atchFilePairName(((MultipartFile)v.get("multipart")).getOriginalFilename().split("\\.")[0])
                                        .regDt(LocalDateTime.now())
                                        .audioInfo((AudioHeader)v.get("audio"))
                                        .build()).collect(Collectors.toSet()))
                        .stream()
                        .filter(v -> AllowFileExt.WAV.equals(v.getAtchFileExt()))
                        .map(v -> LearningDataUploadResponse.builder()
                                .atchFileId(v.getUploadSeq())
                                .projectId(id)
                                .atchFileDisp(v.getAtchFilePairName())
                                .atchFilePairName(v.getAtchFilePairName())
                                .playSec(v.getAudioInfo() != null ? v.getAudioInfo().getTrackLength() : 0)
                                .playSecAvg(v.getAudioInfo() != null ? v.getAudioInfo().getTrackLength() : 0)
                                .smplRate(v.getAudioInfo() != null ? AllowSampleRate.findByValue(Integer.parseInt(v.getAudioInfo().getSampleRate())).toString() : null)
                                .regDt(v.getRegDt().format(DateTimeFormatter.ISO_DATE_TIME))
                                .atchFileType("S") // STT 고정
                                .build())
                        .collect(Collectors.toList());*/
            }else{
                throw new FileNotAllowedException("not allow file - ext mismatch");
            }
        }
        else {
            log.error(logTitle + "failed: Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }

    /**
     * 노이즈 템플릿 목록 조회
     * @param smplRate 샘플링레이트 코드
     * @return lists
     */
    public List<NoiseDataTemplate> getNoiseTemplates(String smplRate){
        return noiseDataTemplateRepository.findBySmplRateEquals(smplRate);
    }

}
