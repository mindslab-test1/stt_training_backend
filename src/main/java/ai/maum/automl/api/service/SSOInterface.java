package ai.maum.automl.api.service;

import ai.maum.automl.api.model.TokenVO;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
@Service
public class SSOInterface {

    @Value("${url.hq}")
    String HQ_HOST;

    final RestTemplate restTemplate;

    private final String token = "/hq/oauth/token2";
    private final String refToken = "/hq/oauth/token";
    private final String valid = "/oauth/validateCode";

    public SSOInterface(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /*
     ** 토큰 발행 요청
     */
    public TokenVO publishTokens(String code, String redirectUri) {
        try {
            String url = HQ_HOST + token;
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            // 데이터
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("grant_type", new StringBody("authorization_code", ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("code", new StringBody(code, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("redirect_uri",new StringBody(redirectUri, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            HttpEntity entity = builder.build();
            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            if (responseCode == 200) {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");
                log.info("response = {}", result);

                ObjectMapper mapper = new ObjectMapper();
                TokenVO token = mapper.readValue(result, TokenVO.class);

                return token;
            }
            else {
                log.error("Response Code : {}", responseCode);
            }
        } catch(Exception e) {
            log.error(e.toString());
        }
        return null;
    }


    /*
     ** 토큰 재발급 요청
     */
    public TokenVO republishTokens(String refreshToken) {
        try {
            String url = HQ_HOST + refToken;
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            // 데이터
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("grant_type", new StringBody("refresh_token", ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("refresh_token", new StringBody(refreshToken, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            HttpEntity entity = builder.build();
            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            if (responseCode == 200) {
                String result = EntityUtils.toString(response.getEntity(), "UTF-8");
                log.info("response = {}", result);

                ObjectMapper mapper = new ObjectMapper();
                TokenVO token = mapper.readValue(result, TokenVO.class);

                return token;
            }
            else {
                log.error("Response Code : {}", responseCode);
            }
        } catch(Exception e) {
            log.error(e.toString());
        }

        return null;
    }

    /**
     * 코드/스테이트로 유효성 검증
     * @param state callback state
     * @param code callback code
     * @return valid true/false
     */
    public boolean validCodeAndState(String state, String code){
        SSOValidateResponse response = restTemplate.getForObject(UriComponentsBuilder.fromHttpUrl(HQ_HOST+valid).queryParam("state", state).queryParam("code", code).build().toUri(), SSOValidateResponse.class);
        if(response.code == 200 && Optional.ofNullable(response.getData()).isPresent() && response.getData().valid){
            return true;
        }
        return false;
    }

    public void cleanTokens(String clientId, String accessToken) {
//        try {
//            String url = HQ_HOST + "/hq/oauth/token";
//            HttpClient client = HttpClients.createDefault();
//            MyHttpDelete deleteMethod = new MyHttpDelete(url);
//
//            // 데이터
//            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
//            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
//            builder.addPart("client_id", new StringBody(clientId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
//            builder.addPart("access_token", new StringBody(accessToken, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
//            HttpEntity entity = builder.build();
//            deleteMethod.setEntity(entity);
//
//            HttpResponse response = client.execute(deleteMethod);
//
//            int responseCode = response.getStatusLine().getStatusCode();
//            if (responseCode == 200) {
//                String result = EntityUtils.toString(response.getEntity(), "UTF-8");
//                log.info("response = {}", result);
//            }
//            else {
//                log.error("Response Code : {}", responseCode);
//            }
//        } catch(Exception e) {
//            log.error(e.toString());
//        }
    }

    @Getter
    @Setter
    static class SSOValidateResponse {

        private int code;
        private String msg;
        private SSOValidateData data;

        @Getter
        @Setter
        static class SSOValidateData {
            private boolean valid;
            private LocalDateTime createCodeTime;
        }

    }

}
