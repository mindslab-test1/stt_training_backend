package ai.maum.automl.api.service;

import ai.maum.automl.api.commons.utils.ModelMapperService;
import ai.maum.automl.api.model.dto.common.response.CodeGroupResponse;
import ai.maum.automl.api.model.dto.common.response.CodeResponse;
import ai.maum.automl.api.model.entity.automl.common.Code;
import ai.maum.automl.api.repository.automl.CodeGroupRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * CodeService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-21
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class CodeService {

    private final ModelMapperService modelMapperService;
    private final CodeGroupRepository codeGroupRepository;

    @PersistenceContext(unitName = "autoMLEntityManager")
    EntityManager entityManager;

    /**
     * 코드그룹 코드로 코드그룹 엔티티 및 코드목록 조회
     * @param codeGroup
     * @return
     */
    public CodeGroupResponse getCodeGroupAndCodes(String codeGroup){
        return Optional.ofNullable(codeGroupRepository.findCodeGroupByCodeGroupEqual(entityManager, codeGroup))
                .map(v -> {
                    CodeGroupResponse codeGroupResponse = CodeGroupResponse.builder()
                            .codeGroup(v.getCodeGroup())
                            .codeGroupName(v.getCodeGroupName())
                            .codes(v.getCodes().stream().sorted(Comparator.comparing(Code::getOrder)).map(sv -> new CodeResponse(sv.getCode(), sv.getCodeName(), sv.getCodeName())).collect(Collectors.toList()))
                            .build();
                    codeGroupResponse.setSuccess(true);
                    codeGroupResponse.setMessage("조회 성공");
                    return codeGroupResponse;
                })
                .orElseThrow(() -> {
                    throw new EmptyResultDataAccessException(0);
                });
    }

}
