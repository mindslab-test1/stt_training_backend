package ai.maum.automl.api.service;

import ai.maum.automl.api.commons.exception.ContainerUnpreparationException;
import ai.maum.automl.api.commons.websocket.ContainerAttachWebsocketUtil;
import ai.maum.automl.api.config.SFTPConfig;
import ai.maum.automl.api.model.dto.project.request.ModelTestRequest;
import ai.maum.automl.api.model.entity.automl.common.User;
import ai.maum.automl.api.model.entity.automl.project.Project;
import ai.maum.automl.api.model.entity.automl.project.ProjectSnapshot;
import ai.maum.automl.api.repository.automl.ProjectRepository;
import ai.maum.automl.api.repository.automl.ProjectSnapshotRepository;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.SftpProgressMonitor;
import com.spotify.docker.client.exceptions.DockerException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.*;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.persistence.EntityNotFoundException;
import java.io.*;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class ModelTestService {

    @Value("${backend.api.url}")
    private String backendApiUrl;

    @Value("${ml.data.root}")
    private String rootPath;

    @Value("${ml.data.test}")
    private String testPath;

    @Value("${ml.data.result}")
    private String resultPath;

    @Value("${ml.data.model}")
    private String modelPath;

    private final SFTPConfig sftpConfig;
    private final ContainerAttachWebsocketUtil containerAttachWebsocketUtil;
    private final Environment environment;
    private final DockerService dockerService;
    private final SimpMessagingTemplate template;
    private final ProjectRepository projectRepository;
    private final ProjectSnapshotRepository projectSnapshotRepository;

    private final String testApiPath = "/api/test";

    /**
     * 모델 테스트 시작
     * @param modelTestRequest
     */
    public void startModelTest(ModelTestRequest modelTestRequest, User user) throws FileUploadException, ContainerUnpreparationException {
        if(uploadFile(modelTestRequest)) {
            try {
                if(dockerService.checkContainerStartCount()) {
                    deleteFile(modelTestRequest);
                    throw new ContainerUnpreparationException("사용가능한 컨테이너가 없습니다.");
                }else {
                    if(modelTestRequest.getProjectSeq() == 0 && modelTestRequest.getSnapshotSeq() == 0){
                        // 사전학습모델일 경우..
                        String path = rootPath + "/model" + testPath;
                        dockerService.exec(
                                "w2l",
                                Arrays.asList(
                                        "sh",
                                        "-c",
                                        "/DATA/AutoML_DATA/TEST/testPre" +
                                                " -r " +
                                                "/DATA/AutoML_DATA/model" +
                                                " -m " +
                                                modelTestRequest.getModelFileName() +
                                                " -s " +
                                                modelTestRequest.getSmplRate() +
                                                " -i " +
                                                path +
                                                " -p " +
                                                "pre_train_"+UUID.randomUUID() +
                                                " -t " +
                                                "/DATA/AutoML_DATA/"+modelTestRequest.getTargetProjectSeq()+"/"+modelTestRequest.getTargetSnapshotSeq()+"/result/"+modelTestRequest.getTargetModelFileName() +
                                                " > /proc/1/fd/1"
                                )
                        );
                        containerAttachWebsocketUtil.attachContainer(
                                "w2l",
                                null,
                                modelTestRequest,
                                null
                        );
                    }else{
                        Optional<Project> project = projectRepository.findByIdEqualsAndUserEquals(modelTestRequest.getProjectSeq(), user);
                        if(project.isPresent()){
                            String path = rootPath + "/" + modelTestRequest.getProjectSeq() + "/" + modelTestRequest.getSnapshotSeq() + testPath;
                            Optional<ProjectSnapshot> snapshotOptional = projectSnapshotRepository.findById(modelTestRequest.getSnapshotSeq());
                            if(snapshotOptional.isPresent()){
                                dockerService.exec(
                                        "w2l",
                                        Arrays.asList(
                                                "sh",
                                                "-c",
                                                "/DATA/AutoML_DATA/TEST/test" +
                                                        " -r " +
                                                        "/DATA/AutoML_DATA/"+modelTestRequest.getProjectSeq()+"/"+modelTestRequest.getSnapshotSeq()+"/result" +
                                                        " -m " +
                                                        modelTestRequest.getModelFileName() +
                                                        " -s " +
                                                        modelTestRequest.getSmplRate() +
                                                        " -i " +
                                                        path +
                                                        " -p " +
                                                        modelTestRequest.getProjectSeq()+"_"+modelTestRequest.getSnapshotSeq() +
                                                        " -t " +
                                                        "/DATA/AutoML_DATA/"+modelTestRequest.getTargetProjectSeq()+"/"+modelTestRequest.getTargetSnapshotSeq()+"/result/"+modelTestRequest.getTargetModelFileName() +
                                                        " > /proc/1/fd/1"
                                            /*"-r",
                                            "/DATA/AutoML_DATA/"+modelTestRequest.getProjectSeq()+"/"+modelTestRequest.getSnapshotSeq()+"/result",
                                            "-m",
                                            modelTestRequest.getModelFileName(),
                                            "-s",
                                            String.valueOf(modelTestRequest.getSmplRate()),
                                            "-i",
                                            path,
                                            "-p",
                                            modelTestRequest.getProjectSeq()+"_"+modelTestRequest.getSnapshotSeq(),
                                            ">",
                                            "/proc/1/fd/1"*/
                                        )
                                );
                                containerAttachWebsocketUtil.attachContainer(
                                        "w2l",
                                        snapshotOptional.get(),
                                        modelTestRequest,
                                        project.get().getProjectType()
                                );
                            }else{
                                throw new EntityNotFoundException("프로젝트 스냅샷 정보를 찾을 수 없습니다.");
                            }
                        }else{
                            throw new EntityNotFoundException("프로젝트 정보를 찾을 수 없습니다.");
                        }
                    }
                }
            } catch (DockerException | InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            deleteFile(modelTestRequest);
            throw new FileUploadException("파일 업로드에 실패하였습니다.");
        }
    }

    /**
     * 파일 업로드 처리
     * @param modelTestRequest
     */
    public boolean uploadFile(ModelTestRequest modelTestRequest){
        String path;
        if(modelTestRequest.getProjectSeq() == 0 && modelTestRequest.getSnapshotSeq() == 0){
            path = rootPath + "/model" + testPath;
        }else{
            path = rootPath + "/" + modelTestRequest.getProjectSeq() + "/" + modelTestRequest.getSnapshotSeq() + testPath;
        }
        boolean uploadCmpl = true;
        if(Arrays.stream(environment.getActiveProfiles()).anyMatch(sv -> sv.equalsIgnoreCase("local"))){
            ChannelSftp sftp = null;
            Session session = null;
            try {
                session = (Session) sftpConfig.connectSFTP().get("session");
                sftp = (ChannelSftp) sftpConfig.connectSFTP().get("channel");
                ChannelSftp finalSftp = sftp;
                for(MultipartFile v : modelTestRequest.getFiles()){
                    InputStream inputStream;
                    try {
                        inputStream = v.getInputStream();
                        String fileName = "/" + v.getOriginalFilename();
                        try {
                            finalSftp.cd(path);
                        } catch (SftpException e) {
                            finalSftp.mkdir(path);
                        }
                        finalSftp.put(inputStream, path+fileName, new SftpProgressMonitor() {

                            private long max = 0;
                            private long count = 0;
                            private long percent = 0;

                            @Override
                            public void init(int op, String src, String dest, long max) {
                                this.max = max;
                            }

                            @Override
                            public void end() {

                            }

                            @Override
                            public boolean count(long bytes) {
                                this.count += bytes;
                                long percentNow = this.count*100/max;
                                if(percentNow>this.percent){
                                    this.percent = percentNow;
                                    log.info("progress : {}%", this.percent);
                                }
                                return true;
                            }

                        });
                    } catch (IOException | SftpException e) {
                        e.printStackTrace();
                        uploadCmpl = false;
                    }
                    if(!uploadCmpl) break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                sftp.disconnect();
                session.disconnect();
            }
        } else {
            for(MultipartFile v : modelTestRequest.getFiles()){
                File file = new File(path+"/"+v.getOriginalFilename());
                try {
                    FileUtils.touch(file);
                    v.transferTo(file);
                } catch (IOException e) {
                    uploadCmpl = false;
                    e.printStackTrace();
                }
                if(!uploadCmpl) break;
            }
        }
        return uploadCmpl;
    }

    /**
     * 파일 삭제
     * @param modelTestRequest
     */
    public void deleteFile(ModelTestRequest modelTestRequest){
        String path = rootPath + "/" + modelTestRequest.getProjectSeq() + "/" + modelTestRequest.getSnapshotSeq() + testPath;
        if(Arrays.stream(environment.getActiveProfiles()).anyMatch(sv -> sv.equalsIgnoreCase("local"))){
            ChannelSftp sftp = null;
            Session session = null;
            try {
                session = (Session) sftpConfig.connectSFTP().get("session");
                sftp = (ChannelSftp) sftpConfig.connectSFTP().get("channel");
                sftp.rmdir(path);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                sftp.disconnect();
                session.disconnect();
            }
        }else{
            new File(path).delete();
        }
    }

    /**
     * 모델 다운로드
     * @param modelTestRequest
     * @param user
     * @return
     * @throws IOException
     */
    public Map<String, Object> downloadModel(ModelTestRequest modelTestRequest, User user) throws IOException {
        Optional<Project> project = projectRepository.findByIdEqualsAndUserEquals(modelTestRequest.getProjectSeq(), user);
        if(project.isPresent()){
            String path = rootPath + "/" + modelTestRequest.getProjectSeq() + "/" + modelTestRequest.getSnapshotSeq() + "/" + resultPath + "/" + modelPath + "/" + modelTestRequest.getModelFileName();
            if(Arrays.stream(environment.getActiveProfiles()).anyMatch(sv -> sv.equalsIgnoreCase("local"))){
                ChannelSftp sftp = null;
                Session session = null;
                try {
                    session = (Session) sftpConfig.connectSFTP().get("session");
                    sftp = (ChannelSftp) sftpConfig.connectSFTP().get("channel");
                    Path temp = Files.createTempFile(null, null);
                    FileUtils.copyInputStreamToFile(sftp.get(path), temp.toFile());
                    temp.toFile().deleteOnExit();
                    return Map.of("size", temp.toFile().length(), "resource", new InputStreamResource(new FileInputStream(temp.toFile())));
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new IOException("파일 다운로드 처리 중 요류가 발생했습니다.");
                } finally {
                    sftp.disconnect();
                    session.disconnect();
                }
            }else{
                Resource resource = new InputStreamResource(new FileInputStream(path));
                if(resource.exists() || resource.isReadable()) {
                    return Map.of("size", new File(path).length(), "resource", resource);
                }else {
                    throw new FileNotFoundException("파일을 찾을 수 없습니다.");
                }
            }
        }else{
            throw new EntityNotFoundException("프로젝트 소유 정보를 찾을 수 없습니다.");
        }
    }

    /**
     * Desc : 모델 테스트 생성 요청
     * */
    public HashMap<String, Object> beginModelTest(String modelId, String modelFileName, String modelFilePath, MultipartFile dataFile) throws ParseException, IOException {
        String logTitle = "beginModelTest/modelId[" + modelId + "] modelFileName[" + modelFileName + "] modelFilePath[" + modelFilePath + "] dataFile[" + dataFile + "]/";
        String apiUrl = backendApiUrl + testApiPath + "/begin";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setAccept(Arrays.asList(MediaType.ALL));

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("modelId", modelId);
        body.add("modelFilePath", modelFilePath);
        body.add("modelFileName", modelFileName);
        body.add("file", dataFile.getResource());
        //body.add("file", new ByteArrayResourceWithFileName(dataFile.getOriginalFilename(), dataFile.getBytes()));

        HttpEntity<MultiValueMap<String, Object>> reqEntity = new HttpEntity<>(body, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(apiUrl, reqEntity, String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            System.out.println(response.toString());
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();

        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        //HashMap<String, Object> responseBody = new Gson().fromJson(jsonObject.toString(), HashMap.class);
        HashMap<String, Object> responseBody = new HashMap<>();

        if("false".equalsIgnoreCase(jsonObject.get("success").toString())) {    // api error
            responseBody.put("success", jsonObject.get("success"));
            responseBody.put("message", jsonObject.get("message"));
            responseBody.put("errorCode", jsonObject.get("errorCode"));
        } else {
            responseBody.put("success", jsonObject.get("success"));
            responseBody.put("status", jsonObject.get("status"));
            responseBody.put("modelTestSnapshotId", jsonObject.get("modelTestSnapshotId"));
            responseBody.put("queueIndex", jsonObject.get("queueIndex"));
        }

        log.info(logTitle + "success");

        return responseBody;
    }

    /**
     * DESC : 대기열에 있거나 실행중인 모델 테스트의 상태값 조회
     */
    public HashMap<String, Object> getModelTestStatus(Long userNo, Long modelTestSnapshotId) throws ParseException {
        String logTitle = "getModelTestStatus/modelTestSnapshotId[" + modelTestSnapshotId + "]/";
        String apiUrl = backendApiUrl + testApiPath + "/status";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
                .queryParam("modelTestSnapshotId", modelTestSnapshotId)
                .build(false);
        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, new HttpEntity<String>(headers), String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        //HashMap<String, Object> responseBody = new Gson().fromJson(jsonObject.toString(), HashMap.class);
        HashMap<String, Object> responseBody = new HashMap<>();

        if("false".equalsIgnoreCase(jsonObject.get("success").toString())) {    // api error
            responseBody.put("success", jsonObject.get("success"));
            responseBody.put("message", jsonObject.get("message"));
            responseBody.put("errorCode", jsonObject.get("errorCode"));
        } else {
            responseBody.put("success", jsonObject.get("success"));
            responseBody.put("status", jsonObject.get("status"));
            responseBody.put("result", jsonObject.get("result"));
            responseBody.put("modelTestSnapshotId", jsonObject.get("modelTestSnapshotId"));
            responseBody.put("queueIndex", jsonObject.get("queueIndex"));
        }

        log.info(logTitle + "success");

        return responseBody;
    }

    /**
     * DESC : 대기열에 있는 모델 테스트 취소
     */
    public HashMap<String, Object> cancelModelTest(Long userNo, Long modelTestSnapshotId) throws ParseException {
        String logTitle = "cancelModelTest/modelTestSnapshotId[" + modelTestSnapshotId + "]/";
        String apiUrl = backendApiUrl + testApiPath + "/cancel";

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("application", "json", Charset.forName("UTF-8")));

        UriComponents builder = UriComponentsBuilder.fromHttpUrl(apiUrl)
                .queryParam("modelTestSnapshotId", modelTestSnapshotId)
                .build(false);
        ResponseEntity<String> response = restTemplate.exchange(builder.toUriString(), HttpMethod.PUT, new HttpEntity<String>(headers), String.class);

        int responseCode = response.getStatusCode().value();
        if (responseCode != 200) {
            log.error(logTitle + "failed : responseCode=" + responseCode);
            throw new RuntimeException();
        }

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = (JSONObject) jsonParser.parse(response.getBody());
        //HashMap<String, Object> responseBody = new Gson().fromJson(jsonObject.toString(), HashMap.class);
        HashMap<String, Object> responseBody = new HashMap<>();

        if("false".equalsIgnoreCase(jsonObject.get("success").toString())) {    // api error
            responseBody.put("success", jsonObject.get("success"));
            responseBody.put("message", jsonObject.get("message"));
            responseBody.put("errorCode", jsonObject.get("errorCode"));
        } else {
            responseBody.put("success", jsonObject.get("success"));
            responseBody.put("message", jsonObject.get("message"));
        }

        log.info(logTitle + "success");

        return responseBody;
    }
}
