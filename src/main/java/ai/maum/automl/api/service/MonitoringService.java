package ai.maum.automl.api.service;

import ai.maum.automl.api.model.dto.monitoring.response.DataProcessingMonitoringResponse;
import ai.maum.automl.api.model.dto.monitoring.response.ServiceMonitoringResponse;
import ai.maum.automl.api.model.dto.monitoring.response.SttProcessingMonitoringResponse;
import ai.maum.automl.api.model.dto.monitoring.ServiceMonitoringDto;
import ai.maum.automl.api.model.entity.stt.DdtctSttRcrdStat;
import ai.maum.automl.api.repository.stt.DdtctSttRcrdStatRepository;
import ai.maum.automl.api.repository.stt.DdtctSttSvcMotrRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * MonitoringService
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-22  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-22
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class MonitoringService {

    private final DdtctSttSvcMotrRepository ddtctSttSvcMotrRepository;
    private final DdtctSttRcrdStatRepository ddtctSttRcrdStatRepository;

    @PersistenceContext(unitName = "sttEntityManager")
    EntityManager entityManager;

    /**
     * 서비스 모니터링 현황
     * @return
     */
    @Transactional(value = "sttTransactionManager", readOnly = true)
    public ServiceMonitoringResponse getServiceMonitoringData(){
        LocalDateTime dateTime = LocalDateTime.now(ZoneId.of("Asia/Seoul"));
        List<ServiceMonitoringDto> list = ddtctSttSvcMotrRepository.findDdtctSttSvcMotrs(entityManager);
        return ServiceMonitoringResponse.builder()
                .svcIds(list.stream().map(v -> Map.of("servId", v.getServId(), "servIdNm", v.getServIdNm())).collect(Collectors.toSet()))
                .servMap(list.stream().map(v -> {
                    v.setSvcStatCd(Duration.between(v.getUpdatedTm(), dateTime).toSeconds() > 60 ? "UNIDENTIFIED" : v.getSvcStatCd());
                    return v;
                }).collect(Collectors.groupingBy(
                        ServiceMonitoringDto::getSvcId,
                        Collectors.toMap(ServiceMonitoringDto::getServId, Function.identity()))
                ))
                .build();
    }

    /**
     * 데이터 처리 모니터링 현황
     * @param startDate 조회 시작일
     * @param endDate 조회 종료일
     * @return
     */
    @Transactional(value = "sttTransactionManager", readOnly = true)
    public DataProcessingMonitoringResponse getDataProcessingMonitoringData(LocalDateTime startDate, LocalDateTime endDate){
        List<DdtctSttRcrdStat> list = ddtctSttRcrdStatRepository.findByDntRgstGreaterThanEqualAndDntRgstLessThanEqualOrderByDntRgstAsc(startDate, endDate);
        int requestTime = list.stream().mapToInt(v -> Optional.ofNullable(v.getTimeRcrdPhon()).isPresent() ? Integer.parseInt(v.getTimeRcrdPhon()) : 0).sum();
        long processingDays = Duration.between(startDate, endDate).toDays();
        long processingTime = list.stream().filter(v -> "13".equals(v.getCdPrgsStat()) || ("19".equals(v.getCdPrgsStat()) && (Optional.ofNullable(v.getCdPrgsStatDtil()).isPresent() && "DLERR01".equals(v.getCdPrgsStatDtil())))).mapToInt(v -> Integer.parseInt(v.getTimeRcrdPhon())).sum();
        return DataProcessingMonitoringResponse.builder()
                .processingDays(processingDays == 0 ? 1 : processingDays+1)
                .requestTime(getTime(requestTime))
                .avgTimePerDay(getTime(processingDays == 0 ? requestTime : requestTime / processingDays))
                .processingTime(getTime(processingTime))
                .build();
    }

    /**
     * STT 처리 모니터링 현황
     * @param startDate 조회 시작일
     * @param endDate 조회 종료일
     * @return
     */
    @Transactional(value = "sttTransactionManager", readOnly = true)
    public SttProcessingMonitoringResponse getSttProcessingMonitoringData(LocalDateTime startDate, LocalDateTime endDate){
        List<DdtctSttRcrdStat> list = ddtctSttRcrdStatRepository.findByDntRgstGreaterThanEqualAndDntRgstLessThanEqualOrderByDntRgstAsc(startDate, endDate);
        Map<String, Long> groupByStatMap = list.parallelStream().collect(Collectors.groupingBy(DdtctSttRcrdStat::getCdPrgsStat, Collectors.counting()));
        return SttProcessingMonitoringResponse.builder()
                .sttRequestCount(list.size())
                .sttWaitingCount(Optional.ofNullable(groupByStatMap.get("10")).orElse(0L))
                .sttProgressCount(Optional.ofNullable(groupByStatMap.get("12")).orElse(0L))
                .sttCompletedCount(Optional.ofNullable(groupByStatMap.get("13")).orElse(0L))
                .sttErrorCount(Optional.ofNullable(groupByStatMap.get("19")).orElse(0L))
                .build();
    }

    @Transactional(value = "sttTransactionManager", readOnly = true)
    public Map<String, Map<String, Long>> getSttProcessingMonitoringDataReport(LocalDateTime startDate, LocalDateTime endDate){
        List<DdtctSttRcrdStat> list = ddtctSttRcrdStatRepository.findByDntRgstGreaterThanEqualAndDntRgstLessThanEqualOrderByDntRgstAsc(LocalDateTime.of(2021, 7, 1, 0, 0, 0), LocalDateTime.of(2021, 7, 31, 23, 59, 59));
        Map<String, Map<String, Long>> groupByStatMap = list.stream()
                .collect(
                        Collectors.groupingBy(v -> v.getDntRgst().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                                Collectors.groupingBy(
                                        v -> v.getDntRgst().format(DateTimeFormatter.ofPattern("HH")) + "~" + v.getDntRgst().plusHours(1).format(DateTimeFormatter.ofPattern("HH")),
                                        Collectors.counting()
                                )
                        )
                );
        Map<String, Long> totalMap = list.stream()
                .collect(
                        Collectors.groupingBy(v -> v.getDntRgst().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
                                Collectors.counting()
                        )
                );
        groupByStatMap.entrySet().forEach(v -> {
            totalMap.entrySet().forEach(sv -> {
                if(v.getKey().equalsIgnoreCase(sv.getKey())){
                    v.getValue().put("total", sv.getValue());
                }
            });
        });
        return groupByStatMap;
    }

    /**
     * sec to hh:mm:ss
     * @param sec 초
     * @return
     */
    private String getTime(long sec){
        return String.format("%02d:%02d:%02d", (sec / 3600), ((sec % 3600) / 60), (sec % 60));
    }

}
