package ai.maum.automl.api.service;

import ai.maum.automl.api.commons.enums.AllowFileExt;
import ai.maum.automl.api.commons.exception.FileNotAllowedException;
import ai.maum.automl.api.commons.utils.FileUtils;
import ai.maum.automl.api.commons.utils.ModelMapperService;
import ai.maum.automl.api.model.dto.project.ProjectDto;
import ai.maum.automl.api.model.dto.project.request.LearningDataUploadRequest;
import ai.maum.automl.api.model.dto.project.response.LearningDataUploadResponse;
import ai.maum.automl.api.model.entity.automl.common.User;
import ai.maum.automl.api.model.entity.automl.project.Project;
import ai.maum.automl.api.model.entity.automl.project.ProjectLm;
import ai.maum.automl.api.model.entity.automl.project.UploadLearningData;
import ai.maum.automl.api.repository.automl.ProjectRepository;
import ai.maum.automl.api.repository.automl.UploadLearningDataRepository;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by kirk@mindslab.ai on 2020-10-20
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class LmProjectService {

    @Value("${ml.data.train}")
    private String trainPath;

    @Value("${ml.data.root}")
    private String rootPath;

    private final ModelMapperService modelMapperService;
    private final ProjectRepository projectRepository;
    private final UploadLearningDataRepository uploadLearningDataRepository;
    private final FileUtils fileUtils;

    @PersistenceContext(unitName = "autoMLEntityManager")
    EntityManager entityManager;

    @Transactional
    public ProjectDto.Lm add(Long userNo, ProjectDto dto) {
        if("LM".equalsIgnoreCase(dto.getProjectType())) {
            User user = new User(userNo);
            ProjectLm projectLm = modelMapperService.map(dto, ProjectLm.class);
            projectLm.setCompletedStep(0);
            projectLm.setUser(user);

            String modelName = userNo.toString() + "_LM_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));

            projectLm.setModelName(modelName);
            projectLm = projectRepository.save(projectLm);

            return modelMapperService.map(projectLm, ProjectDto.Lm.class);
        } else {
            log.error("not supported projectType:" + dto.getProjectType());
            return null;
        }
    }

    @Transactional
    public ProjectDto.Lm get(Long userNo, Long id) {
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            Project project = optional.get();
            if("LM".equalsIgnoreCase(project.getProjectType())){
                ProjectLm projectLm = (ProjectLm)project;
                ProjectDto.Lm dto = modelMapperService.map(projectLm, ProjectDto.Lm.class);
                return dto;
            }
            else {
                return null;
            }
        }
        else {
            throw new EmptyResultDataAccessException(0);
        }
    }
    
    @Transactional
    public void updateStep(Long userNo, Long id, int step){
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            ProjectLm project = (ProjectLm)optional.get();
            project.setCompletedStep(step);
            projectRepository.save(project);
        } else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public void learningData(Long userNo, Long id, ProjectDto.LearningDataInput dto) {
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            ProjectLm project = (ProjectLm)optional.get();
            project.setLanguage(dto.getLanguage());
            project.setEos(dto.getEos());
            project.setLearningDataList(
                    dto.getLearningDataList().stream()
                    .peek(v -> {
                        UploadLearningData data = uploadLearningDataRepository.findById(Long.parseLong(v.getAtchFileId()))
                                .orElseThrow(() -> {
                                    throw new EmptyResultDataAccessException(0);
                                });
                        v.setAtchFileName(data.getAtchFilePairName());
                        v.setAtchFileDisp(data.getAtchFilePairName());
                        v.setLangCd(dto.getLanguage());
                        v.setTotFileCount(2);
                        v.setAtchFilePath(rootPath+trainPath+"/"+id);
                    }).collect(Collectors.toList())
            );
            //TODO: step enum 만들것.
            if(project.getCompletedStep() < 1)
                project.setCompletedStep(1);
            projectRepository.save(project);
            // 미사용 파일 삭제
            // TODO : 배치 작성하여 주기적으로 미사용 파일 삭제해야함.
            uploadLearningDataRepository.updateIsUsedByUpdateSeqIn(entityManager, dto.getLearningDataList().stream().map(v -> Long.parseLong(v.getAtchFileId())).collect(Collectors.toList()));
            uploadLearningDataRepository.deleteByIsUsed(entityManager, false);
        } else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public void trainingData(Long userNo, Long id, ProjectDto.TrainingDataInput dto) {
        Optional<Project> optional  = projectRepository.findById(id);
        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            ProjectLm project = (ProjectLm)optional.get();
            project.setCheckSave(dto.getCheckSave());
            projectRepository.save(project);
        }
        else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    /**
     * 학습 데이터 업로드
     * @param id 프로젝트 ID
     * @param userNo 회원 SEQ
     * @param learningDataUploadRequest 업로드 요청
     * @return
     * @throws FileNotAllowedException
     */
    @Transactional
    public List<LearningDataUploadResponse> learningDataUpload(Long id, Long userNo, LearningDataUploadRequest learningDataUploadRequest) {
        String logTitle = "learningDataUpload/id[" + id + "]/";
        Optional<Project> optional  = projectRepository.findById(id);

        if(optional.isPresent() && optional.get().getUser().getId().equals(userNo)) {
            if(learningDataUploadRequest.getFiles().stream().allMatch(v -> {
                try {
                    return AllowFileExt.findByMimeType(new Tika().detect(v.getInputStream())) && Optional.ofNullable(AllowFileExt.findByValue(v.getOriginalFilename().split("\\.")[1])).isPresent();
                } catch (IOException e) {
                    throw new RuntimeException(e.getMessage());
                }
            })){
                Project project = optional.get();
                return uploadLearningDataRepository.saveAll(learningDataUploadRequest.getFiles().stream()
                        .map(v -> {
                            if(AllowFileExt.WAV.equals(AllowFileExt.findByValue(v.getOriginalFilename().split("\\.")[1]))){
                                throw new FileNotAllowedException("not allow file - ext mismatch");
                            } else {
                                try {
                                    final String lmTrainPath = trainPath+"_lm";
                                    return fileUtils.createTxtFile(UUID.randomUUID().toString(), id, v, learningDataUploadRequest.getLang(), rootPath, lmTrainPath);
                                } catch (IOException e) {
                                    throw Throwables.propagate(e);
                                }
                            }
                        }).map(v -> UploadLearningData.builder()
                                        .project(project)
                                        .atchFilePath(rootPath+"/"+id+trainPath+"_lm/"+((File)v.get("file")).getName())
                                        .atchFileName(((File)v.get("file")).getName())
                                        .atchFileSize(((MultipartFile)v.get("multipart")).getSize())
                                        .atchFileExt(AllowFileExt.findByValue(((MultipartFile)v.get("multipart")).getOriginalFilename().split("\\.")[1]))
                                        .atchFileOrgName(((MultipartFile)v.get("multipart")).getOriginalFilename())
                                        .atchFilePairName(((MultipartFile)v.get("multipart")).getOriginalFilename().split("\\.")[0])
                                        .regDt(LocalDateTime.now())
                                        .build()).collect(Collectors.toList()))
                        .stream()
                        .map(v -> LearningDataUploadResponse.builder()
                                .atchFileId(v.getUploadSeq())
                                .projectId(id)
                                .atchFileDisp(v.getAtchFilePairName())
                                .atchFilePairName(v.getAtchFilePairName())
                                .regDt(v.getRegDt().format(DateTimeFormatter.ISO_DATE_TIME))
                                .atchFileType("L") // LM 고정
                                .build()).collect(Collectors.toList());
            }else{
                throw new FileNotAllowedException("not allow file - ext mismatch");
            }
        }
        else {
            log.error(logTitle + "failed: Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }

}
