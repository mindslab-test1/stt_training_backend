package ai.maum.automl.api.service;

import ai.maum.automl.api.commons.enums.ProjectStatus;
import ai.maum.automl.api.model.dto.project.GraphDto;
import ai.maum.automl.api.model.entity.automl.project.*;
import ai.maum.automl.api.repository.automl.*;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

import static ai.maum.automl.api.config.AsyncConfig.RUNNING_TASK;

@Slf4j
@Service
@RequiredArgsConstructor
public class GraphService {

    private final ProjectRepository projectRepository;
    private final ProjectSnapshotRepository projectSnapshotRepository;
    private final QueueRepository queueRepository;
    private final GraphDataSnapshotRepository graphDataSnapshotRepository;
    private final GraphDataRepository graphDataRepository;
    private final LmTrainingSnapshotRepository lmTrainingSnapshotRepository;
    private final ThreadPoolTaskExecutor executor;
    private final String CONTAINER_NAME_PREFIX = "w2l_train_";

    public GraphDto monitoring(Long userNo, Long projectId) throws EmptyResultDataAccessException {
        String logTitle = "monitoring/userNo[" + userNo + "] projectId[" + projectId + "]/";

        Optional<Project> optionalProject = projectRepository.findById(projectId);

        // 프로젝트가 로그인한 사용자의 것인지 조회
        if(optionalProject.isPresent() && optionalProject.get().getUser().getId().equals(userNo)) {

            Optional<ProjectSnapshot> optionalProjectSnapshot  = projectSnapshotRepository.findTop1ByProjectIdOrderByRegDtDesc(projectId);

            if(optionalProjectSnapshot.isPresent()) {
                ProjectStatus status = optionalProjectSnapshot.get().getStatus();
                if(status.equals(ProjectStatus.CREATE)) {          // 학습 대기 중
                    return queueInfo(optionalProjectSnapshot.get().getProjectId(), optionalProjectSnapshot.get().getId(), "q");
                } else if(status.equals(ProjectStatus.START) || status.equals(ProjectStatus.REPEAT)) {   // 학습 진행 중
                    if("STT".equals(optionalProject.get().getProjectType())){
                        return ongoingGraph(optionalProjectSnapshot.get().getId());
                    }else if("LM".equals(optionalProject.get().getProjectType())){
                        return ongoingGraphLM(optionalProjectSnapshot.get().getId());
                    }
                } else if(status.equals(ProjectStatus.END)) {   // 학습 완료
                    Long projectSnapshotId = optionalProjectSnapshot.get().getId();
                    if("STT".equals(optionalProject.get().getProjectType())){
                        return completedGraph(projectSnapshotId);
                    }else if("LM".equals(optionalProject.get().getProjectType())){
                        return completedGraphLM(projectSnapshotId);
                    }
                }
                if(status.equals(ProjectStatus.ERROR)) {   // 학습 중 에러
                    return errorGraph();
                } else {
                    log.error(logTitle + "failed : unknown status ");
                }
            }
            return null;
        }
        else {
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }

    /**
     * epoch 타임 모니터링
     * @param userNo
     * @param projectId
     * @param snapshotId
     * @return
     * @throws EmptyResultDataAccessException
     */
    public String timeMonitoring(Long projectId, Long snapshotId) throws EmptyResultDataAccessException {
        String logTitle = "monitoring - time / projectId[" + projectId + "]/";
        Optional<ProjectSnapshot> optional = projectSnapshotRepository.findById(snapshotId);
        if(optional.isPresent()){
            return optional.get().getTimePerSession();
        }else{
            log.error(logTitle + "failed : Empty result");
            throw new EmptyResultDataAccessException(0);
        }
    }



    /* queue table 조회하여 순번 알려주기 */
    public GraphDto queueInfo(Long projectId, Long projectSnapshotId, String status) {
        GraphDto graphDto = new GraphDto();

        /*int order = queueRepository.findOrder(projectSnapshotId);
        Optional<ProjectSnapshot> entity = projectSnapshotRepository.findById(projectSnapshotId);

        if(entity.isPresent()) {
            graphDto.setModelId(entity.get().getModelId());
        } else {
            graphDto.setModelId("");
        }*/

        graphDto.setStatus(status);
        graphDto.setSnapshotId(projectSnapshotId);
        int order = 0;
        String taskId = CONTAINER_NAME_PREFIX+projectId+"_"+projectSnapshotId;
        for(String key : RUNNING_TASK.keySet()){
            if(key.equals(taskId)){
                break;
            }else{
                order++;
            }
        }
        graphDto.setOrder(order);

        return graphDto;
    }

    /* graph_data_snapshot 값들 조회 후 조합된 값 알려주기 */
    public GraphDto ongoingGraph(Long projectSnapshotId) {
        GraphDto graphDto = new GraphDto();
        JSONArray jsonArray = new JSONArray();

        List<GraphDataSnapshot.DataPoint> graphDataSnapshotList = graphDataSnapshotRepository.findByProjectSnapshotIdOrderByRegDtAsc(projectSnapshotId);

        if (graphDataSnapshotList == null) {
            throw new EmptyResultDataAccessException(0);
        }

        graphDataSnapshotList.forEach(dataPoint -> {
            String str = dataPoint.getDataPoints();
            try {
                JSONParser jsonParser = new JSONParser();
                JSONObject jsonObject = (JSONObject) jsonParser.parse(str);
                jsonArray.add(jsonObject);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        });

        Optional<ProjectSnapshot> entity = projectSnapshotRepository.findById(projectSnapshotId);

        if(entity.isPresent()) {
            graphDto.setModelId(entity.get().getModelId());
        } else {
            graphDto.setModelId("");
        }

        graphDto.setStatus("t");
        graphDto.setSnapshotId(projectSnapshotId);
        graphDto.setDataPoints(jsonArray);
        graphDto.setTimePerSession(timeMonitoring(entity.get().getProjectId(), projectSnapshotId));
        return graphDto;
    }

    /* graph_data_snapshot 값들 조회 후 조합된 값 알려주기 */
    public GraphDto ongoingGraphLM(Long projectSnapshotId) {
        GraphDto graphDto = new GraphDto();
        Optional<List<LmTrainingSnapshot>> opt = lmTrainingSnapshotRepository.findLmTrainingSnapshotByProjectSnapshotIdEquals(projectSnapshotId);

        if (opt.isEmpty()) {
            throw new EmptyResultDataAccessException(0);
        }

        graphDto.setStatus("t");
        graphDto.setSnapshotId(projectSnapshotId);
        graphDto.setDataPoints(opt.get().stream().map(LmTrainingSnapshot::getLogData).collect(Collectors.joining("\n")));

        return graphDto;
    }

    /* graph_data 값 조회하여 알려주기 */
    public GraphDto completedGraph(Long projectSnapshotId) {
        GraphDto graphDto = new GraphDto();
        JSONArray jsonArray = new JSONArray();

        GraphData graphData = graphDataRepository.findByProjectSnapshotId(projectSnapshotId);

        if(graphData == null) {
            throw new EmptyResultDataAccessException(0);
        }

        String str = graphData.getDataPoints();
        try {
            JSONParser jsonParser = new JSONParser();
            jsonArray = (JSONArray) jsonParser.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Optional<ProjectSnapshot> entity = projectSnapshotRepository.findById(projectSnapshotId);

        if(entity.isPresent()) {
            graphDto.setModelId(entity.get().getModelId());
        } else {
            graphDto.setModelId("");
        }

        graphDto.setStatus("c");
        graphDto.setSnapshotId(graphData.getProjectSnapshotId());
        graphDto.setDataPoints(jsonArray);
        graphDto.setTimePerSession(timeMonitoring(entity.get().getProjectId(), projectSnapshotId));

        return graphDto;
    }

    /**
     * LM 그래프 조회
     * @param projectSnapshotId 스냅샷 ID
     * @return
     */
    public GraphDto completedGraphLM(Long projectSnapshotId) {
        Optional<List<LmTrainingSnapshot>> opt = lmTrainingSnapshotRepository.findLmTrainingSnapshotByProjectSnapshotIdEquals(projectSnapshotId);
        return opt.map(v -> {
            GraphDto graphDto = new GraphDto();
            Optional<ProjectSnapshot> entity = projectSnapshotRepository.findById(projectSnapshotId);
            if(entity.isPresent()) {
                graphDto.setModelId(entity.get().getModelId());
            } else {
                graphDto.setModelId("");
            }
            graphDto.setStatus("c");
            graphDto.setSnapshotId(projectSnapshotId);
            graphDto.setDataPoints(v.stream().map(LmTrainingSnapshot::getLogData).collect(Collectors.joining("\n")));
            return graphDto;
        }).orElseThrow(() -> new EmptyResultDataAccessException(0));
    }

    public GraphDto errorGraph() {
        GraphDto graphDto = new GraphDto();
        graphDto.setStatus("e");
        return graphDto;
    }
}
