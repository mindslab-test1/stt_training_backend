package ai.maum.automl.api.service;

import ai.maum.automl.api.model.dto.project.ModelInquiryDto;
import ai.maum.automl.api.model.entity.automl.common.User;
import ai.maum.automl.api.model.entity.automl.project.ModelInquiry;
import ai.maum.automl.api.repository.automl.ModelInquiryRepository;
import ai.maum.automl.api.repository.automl.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@Service
public class ModelInquiryService {

    @Value("${url.mail}")
    private String mailUrl;
    @Value("${contact.mail}")
    private String toAddr;

    private final ModelInquiryRepository inquiryRepo;
    private final UserRepository userRepo;

    public List<ModelInquiry> getInquiriesByUserNo(Long id) {
        List<ModelInquiry> list = inquiryRepo.findByUserId(id);

        return list;
    }

    public ModelInquiry getInquiryByInquiryId(Long inquiryId) {

        Optional<ModelInquiry> optionalModelInquiry = inquiryRepo.findById(inquiryId);
        if(optionalModelInquiry.isPresent()) {
            return optionalModelInquiry.get();
        } else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    public ModelInquiry getInquiryByModelName(Long userNo, String modelName) {

        Optional<ModelInquiry> optionalModelInquiry = inquiryRepo.findByModelName(modelName);
        if( optionalModelInquiry.isPresent() && optionalModelInquiry.get().getUser().getId().equals(userNo)) {
            return optionalModelInquiry.get();
        } else {
            throw new EmptyResultDataAccessException(0);
        }
    }

    @Transactional
    public ModelInquiry addModelInquiry(ModelInquiryDto dto, Long userNo) {

        String subject = "[autoML] " + dto.getName() + " 님의 문의입니다.";
        String message = "이름 : " + dto.getName() + "<br>" +
                "이메일 : " + dto.getEmail() + "<br>" +
                "연락처 : " + dto.getPhoneNumber() + "<br>" +
                (dto.getModelName() == null ? "" : "모델아이디 : " + dto.getModelName() + "<br>") +
                "문의내용 : " + dto.getInquiryDesc() + "<br><br><br><br>" +
                "autoML에서 보낸 메일입니다.";

        try {
            String sendMailResult = sendPostWithForm(dto.getEmail(), toAddr, subject, message);
            if(sendMailResult == null) log.error("Send mail Failed");
        } catch (Exception e) {
            log.error("Send mail Exception");
            e.printStackTrace();
        }

        ModelInquiry modelInquiry = new ModelInquiry();
        modelInquiry.setName(dto.getName());
        modelInquiry.setPhone(dto.getPhoneNumber());
        modelInquiry.setEmail(dto.getEmail());
        modelInquiry.setInquiryDesc(dto.getInquiryDesc());
        modelInquiry.setModelName(dto.getModelName());
        Optional<User> optionalUser = userRepo.findById(userNo);
        if(optionalUser.isPresent()) {
            modelInquiry.setUser( optionalUser.get() );
        } else {
            throw new EmptyResultDataAccessException(0);
        }
        inquiryRepo.save(modelInquiry);
        return modelInquiry;
    }


    public String sendPostWithForm(String fromAddr, String targetAddr, String subject, String message) throws Exception {

        log.debug(" @ Hello sendPostWithForm ! >> TargetAddr : {}, Subject : {}", targetAddr, subject);

        StringBuilder responseString = new StringBuilder();
        HttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost(mailUrl);
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

        builder.addPart("fromaddr", new StringBody(fromAddr, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
        builder.addPart("toaddr", new StringBody(targetAddr, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
        builder.addPart("subject", new StringBody(subject, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
        builder.addPart("message", new StringBody(message, ContentType.TEXT_PLAIN.withCharset("UTF-8")));

        HttpEntity entity = builder.build();
        post.setEntity(entity);
        HttpResponse response = client.execute(post);

        int responseCode = response.getStatusLine().getStatusCode();
        BufferedReader br;

        if(responseCode == 200) {
            HttpEntity resEntity = response.getEntity();
            br = new BufferedReader(new InputStreamReader(resEntity.getContent(), StandardCharsets.UTF_8));

            log.debug(" @ Complete sendPostWithForm ==> TargetAddr : {}", targetAddr);
        } else {
            return null;
        }
        String inputLine;

        while((inputLine = br.readLine()) != null) {
            responseString.append(inputLine);
        }
        br.close();
        return responseString.toString();
    }

}
