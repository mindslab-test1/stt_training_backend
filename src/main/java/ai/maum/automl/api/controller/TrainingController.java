package ai.maum.automl.api.controller;


import ai.maum.automl.api.commons.security.CurrentUser;
import ai.maum.automl.api.commons.security.UserPrincipal;
import ai.maum.automl.api.model.dto.project.ProjectSnapshotDto;
import ai.maum.automl.api.service.TrainingService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/training")
public class TrainingController {

    private final TrainingService trainingService;

    @ApiOperation(value = "학습시작")
    @PostMapping("/begin")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<Map<String, Object>> beginTraining(@CurrentUser UserPrincipal user,
                                                             @RequestBody ProjectSnapshotDto.Detail projectSnapshot){
        String logTitle = "beginTraining/snapshotId[" + projectSnapshot.getId() + "], trainingId[" + projectSnapshot.getModelId() + "]/";
        log.info(logTitle);
        try{
            Map<String, Object> result = trainingService.beginTraining(user.getUserNo(), projectSnapshot.getId(), projectSnapshot.getModelId());
            log.info(logTitle + "success");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (RuntimeException | ParseException e){
            log.error(logTitle + "failed : " + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @ApiOperation(value = "학습 종료")
    @PostMapping("/finish")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<Map<String, Object>> finishTraining(@CurrentUser UserPrincipal user,
                                                              @RequestBody ProjectSnapshotDto.Detail projectSnapshot){
        String logTitle = "finishTraining/snapshotId[" + projectSnapshot.getId() + "], trainingId[" + projectSnapshot.getModelId() + "]/";
        try{
            Map<String, Object> result = trainingService.finishTraining(user.getUserNo(), projectSnapshot.getId(), projectSnapshot.getModelId());
            log.info(logTitle + "success");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (RuntimeException | ParseException e){
            log.error(logTitle + "failed : " + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @ApiOperation(value = "학습 취소")
    @PostMapping("/cancel")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<Map<String, Object>> cancelTraining(@CurrentUser UserPrincipal user,
                                                              @RequestBody ProjectSnapshotDto.Detail projectSnapshot){
        String logTitle = "cancelTraining/snapshotId[" + projectSnapshot.getId() + "], trainingId[" + projectSnapshot.getModelId() + "]/";
        try{
            Map<String, Object> result = trainingService.cancelTraining(user.getUserNo(), projectSnapshot.getId(), projectSnapshot.getModelId());
            log.info(logTitle + "success");
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (RuntimeException | ParseException e){
            log.error(logTitle + "failed : " + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
