package ai.maum.automl.api.controller;

import ai.maum.automl.api.commons.exception.StackTrace;
import ai.maum.automl.api.commons.security.CurrentUser;
import ai.maum.automl.api.commons.security.UserPrincipal;
import ai.maum.automl.api.model.dto.project.ModelInquiryDto;
import ai.maum.automl.api.model.entity.automl.project.ModelInquiry;
import ai.maum.automl.api.service.ModelInquiryService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class ModelInquiryController {

    private final ModelInquiryService inquiryService;

    @ApiOperation(value = "문의사항 ID로 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "inquiryId", value = "문의사항 ID", dataType = "Long")
    })
    @GetMapping("/inquiry/{inquiryId}")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<ModelInquiry> getInquiryByInquiryId(@PathVariable Long inquiryId) {

        String logTitle = "getInquiryByInquiryId/inquiryId[" + inquiryId + "]/";

        try {
            ModelInquiry modelInquiry = inquiryService.getInquiryByInquiryId(inquiryId);
            log.info(logTitle + "success");
            return new ResponseEntity<>(modelInquiry, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }


    @ApiOperation(value = "모델명으로 사용자별 문의사항 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "modelName", value = "모델명", dataType = "String")
    })
    @GetMapping("/inquiry/model/{modelName}")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<ModelInquiry> getInquiryByModelName(@CurrentUser UserPrincipal user,
                                                              @PathVariable String modelName) throws UnsupportedEncodingException {
        String logTitle = "getInquiryByModelName/modelName[" + modelName + "]/";

        try {
            ModelInquiry modelInquiry = inquiryService.getInquiryByModelName(user.getUserNo(), modelName);
            log.info(logTitle + "success");
            return new ResponseEntity<>(modelInquiry, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }


    @ApiOperation(value = "사용자별 문의사항 조회")
    @GetMapping("/inquiry")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<ModelInquiry>> getInquiriesByUser(@CurrentUser UserPrincipal user) {
        String logTitle = "getInquiriesByUser/";

        try {
            List<ModelInquiry> list = inquiryService.getInquiriesByUserNo(user.getUserNo());
            log.info(logTitle + "success");
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }


    @ApiOperation(value = "문의하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dto", value = "문의할 정보", dataType = "ModelInquiryDto")
    })
    @PostMapping("/inquiry")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<ApiResponse> addInquiry(@CurrentUser UserPrincipal user,
                                                  @RequestBody ModelInquiryDto dto) {
        String logTitle = "inquiryAdd/dto[" + dto.toString() + "]/";

        try {
            ModelInquiry modelInquiry = inquiryService.addModelInquiry(dto, user.getUserNo());
            log.info(logTitle + "InquiryId[{}], UserId[{}], [Success]", modelInquiry.getId(), user.getUserNo());
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed:" + StackTrace.getStackTrace(e));
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
