package ai.maum.automl.api.controller;

import ai.maum.automl.api.commons.payload.ApiResponse;
import ai.maum.automl.api.model.dto.monitoring.response.DataProcessingMonitoringResponse;
import ai.maum.automl.api.model.dto.monitoring.response.ServiceMonitoringResponse;
import ai.maum.automl.api.model.dto.monitoring.response.SttProcessingMonitoringResponse;
import ai.maum.automl.api.service.DockerService;
import ai.maum.automl.api.service.MonitoringService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

/**
 * MonitoringController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-22  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-22
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@PreAuthorize("hasAnyRole('USER')")
@RequestMapping("/api/monitoring")
public class MonitoringController {

    private final MonitoringService monitoringService;
    /**
     * 서비스 모니터링
     * @return
     */
    @ApiOperation(value = "서비스 모니터링")
    @GetMapping("/service")
    public ResponseEntity<ServiceMonitoringResponse> serviceMonitoring() {
        ResponseEntity<ServiceMonitoringResponse> responseEntity;
        String logTitle = "monitoring/service/";
        try {
            responseEntity = new ResponseEntity<>(monitoringService.getServiceMonitoringData(), HttpStatus.OK);
            log.info(logTitle + "success");
            return responseEntity;
        } catch (Exception e){
            e.printStackTrace();
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity(new ApiResponse(false, "처리 중 오류가 발생했습니다."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * 데이터 처리 모니터링
     * @param startDate 조회 시작일
     * @param endDate 조회 종료일
     * @return
     */
    @ApiOperation(value = "데이터 처리 모니터링")
    @GetMapping("/data")
    public ResponseEntity<DataProcessingMonitoringResponse> dataProcessingMonitoring(@RequestParam(value="startDate") String startDate,
                                                                                     @RequestParam(value="endDate") String endDate) {
        String logTitle = "monitoring/data/startDate[" + startDate + "], endDate[" + endDate + "], ";
        ResponseEntity<DataProcessingMonitoringResponse> responseEntity;
        try {
            LocalDateTime stDate = LocalDateTime.of(LocalDate.parse(startDate, DateTimeFormatter.ISO_DATE), LocalTime.MIDNIGHT);
            LocalDateTime edDate = LocalDateTime.of(LocalDate.parse(endDate, DateTimeFormatter.ISO_DATE), LocalTime.MAX);
            responseEntity = new ResponseEntity<>(monitoringService.getDataProcessingMonitoringData(stDate, edDate), HttpStatus.OK);
            log.info(logTitle + "success");
            return responseEntity;
        } catch (DateTimeParseException e){
            log.error(logTitle + "DateTimeParse failed:" + e.getMessage());
            return new ResponseEntity(new ApiResponse(false, "올바르지 않은 날짜형식입니다."), HttpStatus.BAD_REQUEST);
        } catch (Exception e){
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity(new ApiResponse(false, "처리 중 오류가 발생했습니다."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * STT 처리 모니터링
     * @param date 조회일
     * @param startTime 조회 시작시간
     * @param endTime 조회 종료시간
     * @return
     */
    @ApiOperation(value = "STT 처리 모니터링")
    @GetMapping("/stt")
    public ResponseEntity<SttProcessingMonitoringResponse> sttProcessingMonitoring(@RequestParam(value="date") String date,
                                                                                   @RequestParam(value="startTime", required = false) String startTime,
                                                                                   @RequestParam(value="endTime", required = false) String endTime) {
        String logTitle = "monitoring/stt/date[" + date + "], startTime[" + startTime + "], endTime[" + endTime + "], ";
        ResponseEntity<SttProcessingMonitoringResponse> responseEntity;
        try {
            LocalDateTime stDate = LocalDateTime.of(LocalDate.parse(date, DateTimeFormatter.ISO_DATE), Optional.ofNullable(startTime).isPresent() ? LocalTime.parse(startTime) : LocalTime.MIN);
            LocalDateTime edDate = LocalDateTime.of(LocalDate.parse(date, DateTimeFormatter.ISO_DATE), Optional.ofNullable(startTime).isPresent() ? LocalTime.parse(endTime) : LocalTime.MAX);
            responseEntity = new ResponseEntity<>(monitoringService.getSttProcessingMonitoringData(stDate, edDate), HttpStatus.OK);
            log.info(logTitle + "success");
            return responseEntity;
        } catch (DateTimeParseException e){
            log.error(logTitle + "DateTimeParse failed:" + e.getMessage());
            return new ResponseEntity(new ApiResponse(false, "올바르지 않은 날짜형식입니다."), HttpStatus.BAD_REQUEST);
        } catch (Exception e){
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity(new ApiResponse(false, "처리 중 오류가 발생했습니다."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
