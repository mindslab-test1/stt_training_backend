package ai.maum.automl.api.controller;

import ai.maum.automl.api.commons.security.CurrentUser;
import ai.maum.automl.api.commons.security.UserPrincipal;
import ai.maum.automl.api.model.dto.project.request.ModelTestRequest;
import ai.maum.automl.api.service.ModelTestService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/test")
public class ModelTestController {

    private final ModelTestService modelTestService;

    @ApiOperation(value = "모델 테스트 시작")
    @PostMapping("/start")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity startModelTest(@CurrentUser UserPrincipal user, ModelTestRequest modelTestRequest) {
        String logTitle = "start/projectSeq[" + modelTestRequest.getProjectSeq() + "] snapshotSeq[" + modelTestRequest.getSnapshotSeq() + "] modelName[" + modelTestRequest.getModelFileName() + "] lang[" + modelTestRequest.getLang() + "] smplRate[" + modelTestRequest.getSmplRate() + "]/";
        try {
            modelTestService.startModelTest(modelTestRequest, user.getUser());
            log.info(logTitle + "success");
            return new ResponseEntity<>(null, HttpStatus.OK);
        } catch (Exception e) {
            log.error(logTitle + "failed : " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "모델 다운로드")
    @PostMapping("/download")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity downloadModel(@CurrentUser UserPrincipal user, ModelTestRequest modelTestRequest) {
        String logTitle = "downloadModel/projectSeq[" + modelTestRequest.getProjectSeq() + "] modelName[" + modelTestRequest.getModelFileName() + "]/";
        try {
            log.info(logTitle + "success");
            Map<String, Object> resourceInfo = modelTestService.downloadModel(modelTestRequest, user.getUser());
            return ResponseEntity.ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .contentLength((Long) resourceInfo.get("size"))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + modelTestRequest.getModelFileName() + "\"")
                    .body(resourceInfo.get("resource"));
        } catch (Exception e) {
            log.error(logTitle + "failed : " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "모델 테스트 생성 요청")
    @PostMapping("/begin")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "modelId", value = "모델 ID", dataType = "String"),
//            @ApiImplicitParam(name = "modelFileName", value = "모델 파일 이름", dataType = "String"),
//            @ApiImplicitParam(name = "modelFilePath", value = "모델 파일 경로", dataType = "String"),
//            @ApiImplicitParam(name = "dataFile", value = "데이터 파일", dataType = "MultipartFile")
//    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<HashMap<String, Object>> beginModelTest(String modelId, String modelFileName, String modelFilePath, MultipartFile dataFile) {
        String logTitle = "beginModelTest/modelId[" + modelId + "] modelFileName[" + modelFileName + "] modelFilePath[" + modelFilePath + "] dataFile[" + dataFile + "]/";

        try {
            HashMap<String, Object> responseBody = modelTestService.beginModelTest(modelId, modelFileName, modelFilePath, dataFile);
            log.info(logTitle + "success");
            return new ResponseEntity<>(responseBody, HttpStatus.OK);
        } catch (Exception e) {
            log.error(logTitle + "failed : " + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "모델 테스트의 상태값 조회")
    @GetMapping("/status")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "modelTestSnapshotId", value = "스냅샷 ID", dataType = "Long")
    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<HashMap<String, Object>> getModelTestStatus(@CurrentUser UserPrincipal user,
                                                                      Long modelTestSnapshotId) {
        Long userNo = user.getUserNo();
        String logTitle = "getModelTestStatus/user[" + userNo + "] modelTestSnapshotId[" + modelTestSnapshotId + "]/";
        
        try {
            HashMap<String, Object> responseBody = modelTestService.getModelTestStatus(userNo, modelTestSnapshotId);
            log.info(logTitle + "success");
            return new ResponseEntity<>(responseBody, HttpStatus.OK);
        } catch (Exception e) {
            log.error(logTitle + "failed : " + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "대기열에 있거나 실행 중인 모델 테스트 취소")
    @GetMapping("/cancel")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "modelTestSnapshotId", value = "스냅샷 ID", dataType = "Long")
    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<HashMap<String, Object>> cancelModelTest(@CurrentUser UserPrincipal user,
                                                                   Long modelTestSnapshotId) {
        Long userNo = user.getUserNo();
        String logTitle = "cancelModelTest/user[" + userNo + "] modelTestSnapshotId[" + modelTestSnapshotId + "]/";

        try {
            HashMap<String, Object> responseBody = modelTestService.cancelModelTest(userNo, modelTestSnapshotId);
            log.info(logTitle + "success");
            return new ResponseEntity<>(responseBody, HttpStatus.OK);
        } catch (Exception e) {
            log.error(logTitle + "failed : " + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
