package ai.maum.automl.api.controller;

import ai.maum.automl.api.commons.payload.ApiResponse;
import ai.maum.automl.api.model.dto.common.response.CodeGroupResponse;
import ai.maum.automl.api.service.CodeService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * CodeController
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-21
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@PreAuthorize("hasAnyRole('USER')")
@RequestMapping("/api/code")
public class CodeController {

    private final CodeService codeService;

    /**
     * 코드그룹 코드로 코드그룹 엔티티 및 코드목록 조회
     * @param codeGroup 코드 그룹
     * @return
     */
    @ApiOperation(value = "코드그룹 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "codeGroup", value = "그룹 코드", dataType = "String")
    })
    @GetMapping("/{codeGroup}")
    public ResponseEntity<CodeGroupResponse> getCode(@PathVariable String codeGroup) {
        String logTitle = "code/codeGroup[" + codeGroup + "], ";
        ResponseEntity<CodeGroupResponse> responseEntity;
        try {
            responseEntity = new ResponseEntity<>(codeService.getCodeGroupAndCodes(codeGroup), HttpStatus.OK);
            log.info(logTitle + "success");
            return responseEntity;
        } catch (EmptyResultDataAccessException e){
            log.error(logTitle + "not found");
            return new ResponseEntity(new ApiResponse(false, "조회 결과가 존재하지 않습니다."), HttpStatus.NOT_FOUND);
        } catch (Exception e){
            log.error(logTitle + "failed:" + e.getMessage());
            e.printStackTrace();
            return new ResponseEntity(new ApiResponse(false, "처리 중 오류가 발생했습니다."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
