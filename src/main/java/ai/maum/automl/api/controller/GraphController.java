package ai.maum.automl.api.controller;

import ai.maum.automl.api.commons.security.CurrentUser;
import ai.maum.automl.api.commons.security.UserPrincipal;
import ai.maum.automl.api.model.dto.project.GraphDto;
import ai.maum.automl.api.service.GraphService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-24
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/training")
public class GraphController {

    private final GraphService graphService;

    @ApiOperation(value = "학습 상태 모니터링")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projectId", value = "프로젝트 ID", dataType = "Long")
    })
    @GetMapping("/monitoring/{projectId}")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<GraphDto> monitoring(@CurrentUser UserPrincipal user,
                                               @PathVariable Long projectId) {
        String logTitle = "monitoring/userId[" + user.getUserNo() + "] projectId[" + projectId + "]/";
        try {
            GraphDto graphDto = graphService.monitoring(user.getUserNo(), projectId);
            log.info(logTitle + "success");
            return new ResponseEntity<>(graphDto, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
