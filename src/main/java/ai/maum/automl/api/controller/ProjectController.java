package ai.maum.automl.api.controller;

import ai.maum.automl.api.commons.security.CurrentUser;
import ai.maum.automl.api.commons.security.UserPrincipal;
import ai.maum.automl.api.model.dto.project.ProjectDto;
import ai.maum.automl.api.model.dto.project.ProjectSnapshotDto;
import ai.maum.automl.api.model.entity.automl.project.ModelTemplate;
import ai.maum.automl.api.service.ProjectService;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by kirk@mindslab.ai on 2020-10-21
 */
@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/project")
public class ProjectController {

    private final ProjectService projectService;

    @ApiOperation(value = "사용자의 전체 프로젝트 리스트 조회")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "pageable", value = "페이징 정보", dataType = "Pageable")
//    })
    @GetMapping
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<Iterable<ProjectDto.ProjectDtoEx>> list(@CurrentUser UserPrincipal user,
                                                 Pageable pageable) {
        String logTitle = "list/pageable[" + pageable.toString() + "]/";
        Iterable<ProjectDto.ProjectDtoEx> list = projectService.list(user.getUserNo(), pageable);
        log.info(logTitle + "success");
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @ApiOperation(value = "프로젝트 일반 정보")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "프로젝트 ID", dataType = "Long")
    })
    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<ProjectDto> get(@CurrentUser UserPrincipal user,
                                          @PathVariable Long id) {
        String logTitle = "get/id[" + id + "]/";
        try {
            ProjectDto projectDto = projectService.get(user.getUserNo(), id);
            log.info(logTitle + "success");
            return new ResponseEntity<>(projectDto, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "프로젝트 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "프로젝트 ID", dataType = "Long")
    })
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<?> delete(@CurrentUser UserPrincipal user,
                                    @PathVariable Long id) {
        String logTitle = "delete/id[" + id + "]/";

        try {
            projectService.delete(user.getUserNo(), id);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info(logTitle + "success");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "프로젝트 파일 삭제")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uploadSeq", value = "학습 파일 순번", dataType = "Long")
    })
    @DeleteMapping("/file/{uploadSeq}")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<?> deleteFile(@PathVariable Long uploadSeq) {
        String logTitle = "deleteFile/uploadSeq[" + uploadSeq + "]/";
        try {
            projectService.deleteFile(uploadSeq);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        log.info(logTitle + "success");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @ApiOperation(value = "프로젝트(모델) 스냅샷 생성")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "프로젝트 ID", dataType = "Long"),
            @ApiImplicitParam(name = "dto", value = "프로젝트 스냅샷 정보", dataType = "ProjectSnapshotDto")
    })
    @PostMapping("/{id}/snapshot")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<ProjectSnapshotDto.Detail> addSnapshot(@CurrentUser UserPrincipal user,
                                                       @PathVariable Long id,
                                                       @RequestBody ProjectSnapshotDto dto) throws JsonProcessingException {
        String logTitle = "addSnapshot/id[" + id + "] dto[" + dto.toString() + "]/";

        try {
            ProjectSnapshotDto.Detail snapshot = projectService.addSnapshot(user.getUserNo(), id, dto.getName());
            log.info(logTitle + "saved[" + snapshot.getId() + "]");
            return new ResponseEntity<>(snapshot, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "프로젝트(모델) 스냅샷 전체 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "프로젝트 ID", dataType = "Long")
    })
    @GetMapping("/{id}/snapshot")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<ProjectSnapshotDto.Detail>> listSnapshot(@CurrentUser UserPrincipal user,
                                                                 @PathVariable Long id) {
        String logTitle = "listSnapshot/id[" + id + "]/";
        try {
            List<ProjectSnapshotDto.Detail> list = projectService.listSnapshot(user.getUserNo(), id);
            log.info(logTitle + "size[{}]", list.size());
            return new ResponseEntity<>(list, HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @ApiOperation(value = "프로젝트(모델) 스냅샷 변경")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projectId", value = "프로젝트 ID", dataType = "Long"),
            @ApiImplicitParam(name = "snapshotId", value = "스냅샷 ID", dataType = "Long")
    })
    @PatchMapping("/{projectId}/snapshot/{snapshotId}")
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<?> restoreSnapshot(@CurrentUser UserPrincipal user,
                                                                 @PathVariable Long projectId,
                                                                 @PathVariable Long snapshotId) throws JsonProcessingException {
        String logTitle = "restoreSnapshot/projectId[" + projectId + "], snapshotId[" + snapshotId + "]/";
        try {
            projectService.restoreSnapshot(user.getUserNo(), projectId, snapshotId);
            log.info(logTitle + "success");
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (EmptyResultDataAccessException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    @ApiOperation(value = "모델 템플릿 목록 조회")
    @GetMapping("/model")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pre", value = "사전학습모델 여부 (false일 경우 user_id가 null)", dataType = "boolean"),
            @ApiImplicitParam(name = "engnType", value = "엔진타입", dataType = "String"),
            @ApiImplicitParam(name = "langCd", value = "언어코드", dataType = "String"),
            @ApiImplicitParam(name = "smplRate", value = "샘플링레이트 코드", dataType = "String")
    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<ModelTemplate>> getModelTemplates(@CurrentUser UserPrincipal user,
                                                                 Boolean pre,
                                                                 String engnType,
                                                                 String langCd,
                                                                 String smplRate) {
        String logTitle = "getModelTemplates/userId[" + user.getEmail() + "] pre[" + pre + "] engnType[" + engnType + "] langCd[" + langCd + "] smplRate[" + smplRate + "]/";
        ResponseEntity<List<ModelTemplate>> responseEntity;
        try
        {
            responseEntity = new ResponseEntity<>(projectService.getModelTemplates(pre != null && pre ? null : user.getUserNo(), engnType, langCd, smplRate), HttpStatus.OK);
            log.info(logTitle + "success");
            return responseEntity;
        } catch (RuntimeException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "프로젝트별 모델 템플릿 목록 조회")
    @GetMapping("/{projectId}/model")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "projectId", value = "프로젝트 ID", dataType = "Long"),
            @ApiImplicitParam(name = "engnType", value = "엔진타입", dataType = "String"),
            @ApiImplicitParam(name = "langCd", value = "언어코드", dataType = "String"),
            @ApiImplicitParam(name = "smplRate", value = "샘플링레이트 코드", dataType = "String")
    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<ModelTemplate>> getProjectModelTemplates(@CurrentUser UserPrincipal user,
                                                                 @PathVariable("projectId") Long projectId,
                                                                 String engnType,
                                                                 String langCd,
                                                                 String smplRate) {
        String logTitle = "getProjectModelTemplates/userId[" + user.getEmail() + "] projectId[" + projectId + "] engnType[" + engnType + "] langCd[" + langCd + "] smplRate[" + smplRate + "]/";
        ResponseEntity<List<ModelTemplate>> responseEntity;
        try
        {
            responseEntity = new ResponseEntity<>(projectService.getProjectModelTemplates(user.getUserNo(), projectId, engnType, langCd, smplRate), HttpStatus.OK);
            log.info(logTitle + "success");
            return responseEntity;
        } catch (RuntimeException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "테스트 타겟 모델 템플릿 목록 조회")
    @GetMapping("/{engnType}/{langCd}/model")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "engnType", value = "엔진타입", dataType = "String"),
            @ApiImplicitParam(name = "langCd", value = "언어코드", dataType = "String"),
    })
    @PreAuthorize("hasAnyRole('USER')")
    public ResponseEntity<List<ModelTemplate>> getProjectModelTemplates(@CurrentUser UserPrincipal user,
                                                                        @PathVariable String langCd,
                                                                        @PathVariable String engnType) {
        String logTitle = "getProjectModelTemplates/userId[" + user.getEmail() + "] langCd[" + langCd + "] engnType[" + engnType + "]/";
        ResponseEntity<List<ModelTemplate>> responseEntity;
        try
        {
            responseEntity = new ResponseEntity<>(projectService.getTargetModelTemplates(user.getUserNo(), engnType, langCd), HttpStatus.OK);
            log.info(logTitle + "success");
            return responseEntity;
        } catch (RuntimeException e) {
            log.error(logTitle + "failed:" + e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }



}
