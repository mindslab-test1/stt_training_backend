package ai.maum.automl.api.model.dto.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

public class UserDto {

    public static class Simple
    {
        private String uuid;

        private String name;
    }

    public static class Base
    {
        private String uuid;

        private String name;

        @JsonIgnore
        private String email;
    }

    @Getter
    public static class ReqUpdate
    {
        private Long id;

        private String name;
    }
}
