package ai.maum.automl.api.model.entity.automl.project;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "GRAPH_DATA", indexes = {@Index(columnList = "projectSnapshotId")})
public class GraphData {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    private String dataPoints;

    private int finalEpoch;

    private BigDecimal bestter;

    private int bestterepoch;

    private BigDecimal bestwer;

    private int bestwerepoch;

    private Long projectSnapshotId;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt;
}
