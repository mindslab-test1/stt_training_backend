package ai.maum.automl.api.model.entity.automl.project;

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "GRAPH_DATA_SNAPSHOT", indexes = {@Index(columnList = "projectSnapshotId")})
public class GraphDataSnapshot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    private String dataPoints;

    private int epoch;

    private Long projectSnapshotId;

    private BigDecimal bestTERScore = new BigDecimal(100);

    private BigDecimal bestWERScore = new BigDecimal(100);

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt;

    public static interface DataPoint {
        String getDataPoints();
    }
}

