package ai.maum.automl.api.model.dto.monitoring;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * ServiceMonitoringDto
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-22  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-22
 */
@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ServiceMonitoringDto implements Serializable {

    private static final long serialVersionUID = 411778288495210169L;

    private String servId; // 서버ID
    private String servIdNm; // 서버ID NM
    private String svcId; // 서비스ID
    private String svcIdNm; // 서비스ID NM
    private String svcStatCd; // 서비스상태코드
    private String oprtDate; // 가동시간
    private String svcStDate; // 서비스시작시간
    private String svcEdDate; // 서비스종료시간
    private LocalDateTime updatedTm; // 수정일시
    private boolean tooltip; // 프론트 tooltip disp

}
