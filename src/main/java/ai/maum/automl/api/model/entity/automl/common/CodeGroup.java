package ai.maum.automl.api.model.entity.automl.common;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

/**
 * CodeGroup
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-21
 */
@Entity
@NoArgsConstructor
@Getter
@Table(name = "CODE_GROUP")
public class CodeGroup {

    @Id
    @Column(length = 24, nullable = false)
    String codeGroup;

    @Column(length = 24, nullable = false)
    String codeGroupName;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "codeGroup", cascade = CascadeType.ALL, orphanRemoval = true)
    Set<Code> codes;


}
