package ai.maum.automl.api.model.entity.stt;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * DdtctSttSvcMotr entity id class
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-22  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-22
 */
@NoArgsConstructor
@EqualsAndHashCode
public class DdtctSttSvcMotrId implements Serializable {

    private static final long serialVersionUID = 781535273007635922L;

    private String servId; // 서버ID
    private String svcId; // 서비스ID

}