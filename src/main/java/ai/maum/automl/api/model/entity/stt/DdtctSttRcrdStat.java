package ai.maum.automl.api.model.entity.stt;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * DdtctSttRcrdStat
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-22  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-22
 */
@Getter
@Entity
@Table(name = "DDTCT_STT_RCRD_STAT")
@IdClass(DdtctSttRcrdStatId.class)
@EqualsAndHashCode
public class DdtctSttRcrdStat implements Serializable {

    private static final long serialVersionUID = 5066453686494531758L;

    @Id
    private Long idRcrd; // 녹취일련번호

    @Id
    @Column(length = 20)
    private String cdObttPth; // 녹취파일 입수경로 코드

    @Id
    private Long seqRcrd; // 녹취순서

    @Column(length = 20)
    private String cdPrgsStat; // STT 진행상태 코드

    @Column(length = 20)
    private String cdPrgsStatDtil; // STT 진행상태 상세 코드

    @Column(length = 8)
    private String dtRcrdBegn; // 녹취시작일자

    @Column(length = 10)
    private String dntRcrdBegn; // 녹취시작일시

    @Column(length = 10)
    private String dntRcrdEnd; // 녹취종료일시

    @Column(length = 5)
    private String timeRcrdPhon; // 녹취통화시간

    @Column(length = 5)
    private String timeRcrdIntg; // 통합본통화시간

    @Column(length = 1)
    private String clIbndObnd; // I/O 여부

    private LocalDateTime dntRcrdTrsm; // 녹취전송일시

    @Column(length = 1)
    private String ynSpkPttn; // 스테레오분할 여부

    @Column(length = 20)
    private String cdCnnlCl; // STT 채널구분 코드

    @Column(length = 30)
    private String nmAnssHndlSvr; // STT처리 서버명

    @Column(length = 10)
    private String vluAnssHndl; // STT처리 속도

    private LocalDateTime dntAnssBegn; // STT시작 일시

    private LocalDateTime dntAnssCplt; // STT종료 일시

    @Column(length = 10)
    private String vluCstmVoceSped; // 고객 음성 속도 (클라이언트 음성 속도)

    @Column(length = 10)
    private String vluCnsrVoceSped; // 상담원 음성 속도 (에이전트 음성 속도)

    @Column(length = 10)
    private String timeMaxNotgVoce; // 최대무음

    private LocalDateTime dntRgst; // 등록일시

    @Column(length = 20)
    private String idRgst; // 등록아이디

    @Column(length = 15)
    private String ipRgst; // 등록아이피

    private LocalDateTime dntChg; // 변경일시

    @Column(length = 20)
    private String idChg; // 변경아이디

    @Column(length = 15)
    private String ipChg; // 변경아이피

}