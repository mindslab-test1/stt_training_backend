package ai.maum.automl.api.model.entity.stt;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * DdtctSttRcrdStat entity id class
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-22  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-22
 */
@NoArgsConstructor
@EqualsAndHashCode
public class DdtctSttRcrdStatId implements Serializable {

    private static final long serialVersionUID = 1046291870897272658L;

    private Long idRcrd; // 녹취일련번호
    private String cdObttPth; // 녹취파일 입수경로 코드
    private Long seqRcrd; // 녹취순서

}