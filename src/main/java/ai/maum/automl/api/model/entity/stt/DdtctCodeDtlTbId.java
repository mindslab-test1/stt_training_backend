package ai.maum.automl.api.model.entity.stt;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * DdtctCodeDtlTbId entity id class
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-30  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-30
 */
@NoArgsConstructor
@EqualsAndHashCode
public class DdtctCodeDtlTbId implements Serializable {

    private static final long serialVersionUID = 4426860754330448477L;

    private String cdGp; // 코드그룹
    private String cd; // 코드

}