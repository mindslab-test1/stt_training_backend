package ai.maum.automl.api.model.dto.monitoring.response;

import lombok.*;

import java.io.Serializable;

/**
 * DataProcessingMonitoringResponse
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-22  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-22
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class DataProcessingMonitoringResponse implements Serializable {

    private static final long serialVersionUID = -7114692960790226738L;

    long processingDays;
    String avgTimePerDay;
    String requestTime;
    String processingTime;

}
