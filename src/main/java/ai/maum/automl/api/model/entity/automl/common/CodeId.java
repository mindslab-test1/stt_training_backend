package ai.maum.automl.api.model.entity.automl.common;

import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Code Entity Id Class
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-21
 */
@NoArgsConstructor
@EqualsAndHashCode
public class CodeId implements Serializable {

    private static final long serialVersionUID = 6945078453672592127L;

    private String codeGroup; // 코드그룹
    private String code; // 코드

}
