package ai.maum.automl.api.model.dto.common.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * CodeGroupDto
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-21
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CodeResponse {

    String code;
    String codeName;
    String codeDisp;

}

/*
0: {code: "LN_KO", codeName: "ko", codeDisp: "한국어"}
        code: "LN_KO"
        codeDisp: "한국어"
        codeName: "ko"
        1: {code: "LN_EN", codeName: "en", codeDisp: "English"}
        code: "LN_EN"
        codeDisp: "English"
*/

