package ai.maum.automl.api.model.dto.common;

import lombok.Getter;

@Getter
public class LoginDto {

    private String id;
    private String password;

}
