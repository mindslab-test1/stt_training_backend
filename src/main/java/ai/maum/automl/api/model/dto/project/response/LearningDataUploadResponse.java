package ai.maum.automl.api.model.dto.project.response;

import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * ai.maum.automl.frontapi.model.dto.project.response
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-20  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-20
 */
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public class LearningDataUploadResponse {

    private Long projectId;
    private Long atchFileId;
    private Long id;
    private String atchFileDisp;
    private String atchFileType;
    private String atchFilePairName;
    private String langCd;
    private float playSec;
    private float playSecAvg;
    private String smplRate;
    private String regDt;

}
