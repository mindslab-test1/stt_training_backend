package ai.maum.automl.api.model.entity.automl.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "LM_TRAINING_SNAPSHOT")
public class LmTrainingSnapshot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // VF_CHARACTER 관리번호 FK
    @Column(name = "project_snapshot_id", insertable = false, updatable = false)
    private Long projectSnapshotId;

    @Column
    private String logData;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt;

    @JsonIgnore
    @ManyToOne
    @JoinColumns(
            foreignKey = @ForeignKey(name = "lm_training_snapshot_project_snapshot_fk"),
            value = {
                    @JoinColumn(name = "project_snapshot_id", referencedColumnName = "id"),
            }
    )
    ProjectSnapshot projectSnapshot;

}

