package ai.maum.automl.api.model.entity.automl.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-24
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "queue", indexes = {@Index(columnList = "snapshotId")})
public class Queue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long snapshotId;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    private LocalDateTime regDt;
}
