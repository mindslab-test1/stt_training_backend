package ai.maum.automl.api.model.dto.project;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by kirk@mindslab.ai on 2020-10-22
 */
@Getter
@Setter
@NoArgsConstructor
@ToString
public class ProjectSnapshotDto implements Serializable {
    private String name;

    @Getter
    @Setter
    @NoArgsConstructor
    @ToString
    @EqualsAndHashCode(callSuper = true)
    @FieldDefaults(level = AccessLevel.PRIVATE)
    public static class Detail extends ProjectSnapshotDto {

        private Long id;

        private String snapshot;

        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss", timezone = "Asia/Seoul")
        private LocalDateTime regDt;

        private String modelId;

        private String status;

        private Float bestTer;

        private int bestTerEpoch;

        private Float bestWer;

        private int bestWerEpoch;
    }
}
