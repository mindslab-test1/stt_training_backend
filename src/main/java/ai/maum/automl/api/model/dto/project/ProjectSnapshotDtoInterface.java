package ai.maum.automl.api.model.dto.project;

import java.time.LocalDateTime;

public interface ProjectSnapshotDtoInterface {
    String getName();
    Long getId();
    String getSnapshot();
    LocalDateTime getRegDt();
    String getModelId();
    String getStatus();
    Float getBestTer();
    Integer getBestTerEpoch();
    Float getBestWer();
    Integer getBestWerEpoch();
}
