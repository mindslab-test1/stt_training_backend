package ai.maum.automl.api.model.entity.automl.common;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_USER_EX1,
    ROLE_USER_EX2
}
