package ai.maum.automl.api.model.entity.automl.project;

import ai.maum.automl.api.commons.enums.AllowFileExt;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.jaudiotagger.audio.AudioHeader;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * UploadLearningData Entity
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-23  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-23
 */
@Entity
@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "UPLOAD_LEARNING_DATA", indexes = {
        @Index(name = "upload_learning_data_index_01", columnList = "id,atchFilePairName")
})
public class UploadLearningData {

    @Column
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long uploadSeq;

    @Column(updatable = false, insertable = false, nullable = false)
    private Long id;

    @Column(nullable = false)
    private String atchFilePath;

    @Column(nullable = false)
    private String atchFileName;

    @Column(nullable = false)
    private String atchFileOrgName;

    @Column
    private double atchFileSize;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private AllowFileExt atchFileExt;

    @Column(nullable = false)
    private String atchFilePairName;

    @Column
    private boolean isUsed;

    @Column
    private LocalDateTime regDt;

    @Transient
    private AudioHeader audioInfo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id")
    @JsonIgnore
    Project project;

}
