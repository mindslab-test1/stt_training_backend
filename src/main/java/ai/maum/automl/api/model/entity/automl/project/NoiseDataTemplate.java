package ai.maum.automl.api.model.entity.automl.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * NoiseDataTemplate Entity
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-26  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-26
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class NoiseDataTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Long noiseSeq; // 노이즈 순번

    private String atchFileId; // 파일 아이디

    private String atchFileDisp; // 파일 표시명

    private String atchFilePath; // 파일 경로

    private String atchFileName; // 파일명

    private String orgFileName; // 원본파일명

    private String atchFileType; // 파일유형

    private long   atchFileSize; // 파일크기

    private String smplRate; // 샘플레이트(코드)

    private float playSec; // 재생시간

    private float playSecAvg; // 평균 재생시간

    private float minPlaySec; // 최소 파일 재생시간

    private float maxPlaySec; // 최대 파일 재생시간

    private int totFileCount; // 총 파일 개수

    private String langCd; // 언어

    private String useEosYn; // EOS 적용 여부

    private String engn_type; // 엔진 타입

    @Column(nullable = false, updatable = false)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @CreationTimestamp
    private LocalDateTime regDt; // 생성일시

}
