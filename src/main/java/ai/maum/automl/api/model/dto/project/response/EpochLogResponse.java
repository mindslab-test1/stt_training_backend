package ai.maum.automl.api.model.dto.project.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EpochLogResponse {

    private int epoch;
    private List<EpochLogResponse.Point> data;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Point {
        private String graphType;
        private double train;
        private double dev;

        public Point(String graphType){
            this.graphType = graphType;
        }

    }

}
