package ai.maum.automl.api.model.dto.project.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * ai.maum.automl.frontapi.model.dto.project.request
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-20  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-20
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class LearningDataUploadRequest {

    private String lang;
    private List<MultipartFile> files;

}
