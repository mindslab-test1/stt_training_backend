package ai.maum.automl.api.model.dto.monitoring.response;

import ai.maum.automl.api.model.dto.monitoring.ServiceMonitoringDto;
import lombok.*;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * ServiceMonitoringResponse
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-22  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-22
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class ServiceMonitoringResponse implements Serializable {

    private static final long serialVersionUID = -6583700422488187288L;

    Set<Map<String, String>> svcIds;
    Map<String, Map<String, ServiceMonitoringDto>> servMap;

}
