package ai.maum.automl.api.model.entity.automl.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DiscriminatorValue("LM")
public class ProjectLm extends Project {

    private int completedStep;

    // data setting
    private String language;
    private char eos;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<LearningData> learningDataList = null;

    public void setLearningDataList(List<LearningData> list) {
        if(this.learningDataList == null) {
            this.learningDataList = new ArrayList<>();
        }
        else {
            this.learningDataList.clear();
        }
        if(list == null) return;
        this.learningDataList.addAll(list);
        for (LearningData t: this.learningDataList) {
            if(t.getProject() != this)
                t.setProject(this);
        }
    }

    private int checkSave;

}
