package ai.maum.automl.api.model.entity.stt;

import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * DdtctCodeDtlTb
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-30  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-30
 */
@Getter
@Table(name = "DDTCT_CODE_DTL_TB")
@Entity
public class DdtctCodeDtlTb implements Serializable {

    private static final long serialVersionUID = -6435867606170617820L;

    @Id
    @Column(length = 20)
    private String cdGp; // 코드그룹

    @Column(length = 50)
    private String cd; // 코드

    @Column(length = 100)
    private String cdNm; // 코드명

    @Column(length = 300)
    private String cdDesc; // 코드설명

    @Column(length = 100)
    private String updatorId; // 수정자아이디

    private LocalDateTime updatedTm; // 수정일시

    @Column(length = 100)
    private String creatorId; // 생성자아이디

    private LocalDateTime createdTm; // 생성일시

}