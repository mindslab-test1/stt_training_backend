package ai.maum.automl.api.model.entity.automl.common;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Code
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-21
 */
@Entity
@NoArgsConstructor
@Getter
@IdClass(CodeId.class)
@Table(name = "CODE")
public class Code {

    @Id
    @Column(length = 24, nullable = false)
    String codeGroup; // 코드 그룹

    @Id
    @Column(length = 24, nullable = false)
    String code; // 코드

    @Column(length = 24, nullable = false)
    String codeName; // 코드 이름

    @Column
    int order; // order

}
