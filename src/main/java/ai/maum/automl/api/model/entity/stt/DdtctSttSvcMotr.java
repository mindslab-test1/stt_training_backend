package ai.maum.automl.api.model.entity.stt;

import lombok.Getter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * DdtctSttSvcMotr
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-22  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-22
 */
@Getter
@Entity
@IdClass(DdtctSttSvcMotrId.class)
@Table(name = "DDTCT_STT_SVC_MOTR")
public class DdtctSttSvcMotr implements Serializable {

    private static final long serialVersionUID = 3172323210221527971L;

    @Id
    @Column(length = 50)
    private String servId; // 서버ID

    @Id
    @Column(length = 50)
    private String svcId; // 서비스ID

    @Column(length = 10)
    private String svcStatCd; // 서비스상태코드

    @Column(length = 10)
    private String pssId; // 프로세스ID

    @Column(length = 50)
    private String oprtDate; // 가동시간

    @Column(length = 50)
    private String svcStDate; // 서비스시작시간

    @Column(length = 50)
    private String svcEdDate; // 서비스종료시간

    @Column(length = 100)
    private String updatorId; // 수정자아이디

    private LocalDateTime updatedTm; // 수정일시

    @Column(length = 100)
    private String creatorId; // 생성자아이디

    private LocalDateTime createdTm; // 생성일시

}