package ai.maum.automl.api.model.entity.automl.project;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * ModelTemplate Entity
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-28  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-28
 */
@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ModelTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long modelId; // 모델 순번

    @Column
    private Long projectSeq; // 프로젝트 순번

    @Column
    private Long snapshotSeq; // 스냅샷 순번

    @Column(nullable = false)
    private String modelName; // 모델 이름

    @Column(length = 12, nullable = false)
    private String engnType; // 엔진 타입

    @Column(length = 12, nullable = false)
    private String langCd; // 언어

    @Column(nullable = false)
    private String modelPath; // 모델 파일경로

    @Column(nullable = false)
    private String modelFileName; // 모델 파일이름

    @Column(nullable = false)
    private String lastModelFileName; // 모델 파일이름

    @Column(length = 12)
    private String smplRate; // 샘플링레이트

    @Column(nullable = false, updatable = false)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @CreationTimestamp
    private LocalDateTime regDt; // 등록일시

    @Column(length = 1)
    private String useEosYn; // EOS적용여부

    private Long userSeq; // 회원 순번

}
