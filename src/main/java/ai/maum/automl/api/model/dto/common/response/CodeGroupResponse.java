package ai.maum.automl.api.model.dto.common.response;

import ai.maum.automl.api.commons.payload.ApiResponse;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * CodeGroupDto
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-21
 */
@Getter
@Setter
@Builder
@ToString
@EqualsAndHashCode(callSuper = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CodeGroupResponse extends ApiResponse {

    String codeGroup;
    String codeGroupName;
    List<CodeResponse> codes;

}
