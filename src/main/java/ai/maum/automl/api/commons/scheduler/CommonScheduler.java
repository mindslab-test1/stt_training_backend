package ai.maum.automl.api.commons.scheduler;

import ai.maum.automl.api.service.DockerService;
import ai.maum.automl.api.service.ProjectService;
import com.spotify.docker.client.exceptions.DockerException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * ai.maum.automl.api.commons.scheduler
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-10-21
 */
@Log4j2
@Component
@RequiredArgsConstructor
public class CommonScheduler {

    private final ProjectService projectService;
    private final DockerService dockerService;

    @Scheduled(cron = "0 0 0 * * *", zone = "Asia/Seoul")
    public void deleteNotUsedFiles(){
        log.info("DELETE FILES : NOT USED");
        projectService.deleteNotUsedFiles();
    }

    @Scheduled(cron = "0 0 0 * * *", zone = "Asia/Seoul")
    public void deleteIdleContainer() throws DockerException, InterruptedException {
        dockerService.getIdleContainers().forEach(v -> {
            try {
                log.info("REMOVE CONTAINER : {}", v.id());
                dockerService.deleteContainer(v.id());
            } catch (DockerException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

}
