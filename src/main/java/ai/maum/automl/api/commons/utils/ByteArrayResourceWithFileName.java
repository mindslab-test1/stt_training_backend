package ai.maum.automl.api.commons.utils;

import org.springframework.core.io.ByteArrayResource;

/**
 * Created by kirk on 2020/12/10.
 */
public class ByteArrayResourceWithFileName extends ByteArrayResource {
    private String fileName;

    public ByteArrayResourceWithFileName(String fileName, byte[] byteArray) {
        super(byteArray);
        this.fileName = fileName;
    }

    @Override
    public String getFilename() {
        return fileName;
    }
}
