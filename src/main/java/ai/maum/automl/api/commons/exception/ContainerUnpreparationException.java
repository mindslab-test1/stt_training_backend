package ai.maum.automl.api.commons.exception;

/**
 * 컨테이너가 사용중일때 발생되는 예외
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-09-16  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-09-16
 */
public class ContainerUnpreparationException extends Exception {

    public ContainerUnpreparationException(String msg) {
        super(msg);
    }

}
