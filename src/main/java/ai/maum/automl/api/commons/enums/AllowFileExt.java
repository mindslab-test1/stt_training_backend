package ai.maum.automl.api.commons.enums;

/**
 * ALLOW FILE EXT
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-23  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-23
 */
public enum AllowFileExt {

    WAV,
    TXT;

    public static AllowFileExt findByValue(String value){
        for(AllowFileExt v : values()){
            if( v.name().equalsIgnoreCase(value)){
                return v;
            }
        }
        return null;
    }

    public static boolean findByMimeType(String mimeType){
        if("text/plain".equals(mimeType)){
            mimeType = "txt";
        }
        for(AllowFileExt v : values()){
            if(mimeType.toLowerCase().contains(v.name().toLowerCase())){
                return true;
            }
        }
        return false;
    }

    public static String findExt(String fileName){
        for(AllowFileExt v : values()){
            if(fileName.split("\\.")[1].equalsIgnoreCase(v.name())){
                return "."+fileName.split("\\.")[1];
            }
        }
        return null;
    }

}
