package ai.maum.automl.api.commons.websocket;

import ai.maum.automl.api.commons.enums.ProjectStatus;
import ai.maum.automl.api.model.dto.project.request.ModelTestRequest;
import ai.maum.automl.api.model.dto.project.response.EpochLogResponse;
import ai.maum.automl.api.model.entity.automl.project.*;
import ai.maum.automl.api.repository.automl.*;
import ai.maum.automl.api.service.DockerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketHttpHeaders;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static ai.maum.automl.api.config.AsyncConfig.RUNNING_TASK;

/**
 * docker container attach websocket util
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-08-02
 */
@Component
@Log4j2
@RequiredArgsConstructor
public class ContainerAttachWebsocketUtil {

    private final ProjectSnapshotRepository projectSnapshotRepository;
    private final ProjectRepository projectRepository;
    private final GraphDataSnapshotRepository graphDataSnapshotRepository;
    private final GraphDataRepository graphDataRepository;
    private final LmTrainingSnapshotRepository lmTrainingSnapshotRepository;
    private final ModelTemplateRepository modelTemplateRepository;
    private final DockerService dockerService;
    private final SimpMessagingTemplate template;
    private final String CONTAINER_NAME_PREFIX = "w2l_train_";

    @Value("${ml.host}")
    private String host;

    @PersistenceContext(unitName = "autoMLEntityManager")
    EntityManager entityManager;


    /**
     * 컨테이너 attach, log message에 따라 log insert 및 project 상태 변경
     * @param containerName
     * @param project
     */
    public void attachContainer(String containerName, ProjectSnapshot project, ModelTestRequest modelTestRequest, String projectType){
        try {
            if(Optional.ofNullable(modelTestRequest).isPresent()){
                attachTestingContainer(containerName, modelTestRequest);
            }else{
                attachTrainingContainer(containerName, project, projectType);
            }
        } catch (InterruptedException | ExecutionException e) {
            log.error("DOCKER CLIENT - CONTAINER NAME {} CONNECTION FAIL. CONNECT AGAIN AFTER 3 SECONDS.", containerName);
            // 3 sec repeat
            new ThreadPoolExecutor(0, 1, 0L, TimeUnit.SECONDS, new SynchronousQueue<>())
                    .execute(new FutureTask<>(() -> {
                        Thread.sleep(3000);
                        attachContainer(containerName, project, modelTestRequest, projectType);
                        return null;
                    }));
        }
    }

    /**
     * TEST 컨테이너 attach
     * @param containerName
     * @param project
     * @param modelTestRequest
     * @throws ExecutionException
     * @throws InterruptedException
     */
    public void attachTestingContainer(String containerName, ModelTestRequest modelTestRequest) throws ExecutionException, InterruptedException {
        // --- docker sdk status check ---
        // status 137 - docker stop
        // status 1 - keyboard interrupt
        // status 0 - 정상종료, 에러
        new StandardWebSocketClient().doHandshake(new TextWebSocketHandler() {
            @Override
            public void afterConnectionEstablished(WebSocketSession session) {
                log.info("DOCKER CLIENT - TESTING CONTAINER NAME {} CONNECTION", containerName);
            }

            @SneakyThrows
            @Override
            public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
                log.info("DOCKER CLIENT - TESTING CONTAINER NAME {} CLOSE SESSION", containerName);
                session.close();
            }

            @SneakyThrows
            @Override
            protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
                String msg = new String(message.getPayload().array(), StandardCharsets.UTF_8);
                log.info("MESSAGE : {}", msg);
                template.convertAndSend("/ws/subscribe/test/"+modelTestRequest.getProjectSeq()+"/"+modelTestRequest.getSnapshotSeq(), msg);
                if(msg.contains("complete")){
                    session.close();
                }
            }
        }, new WebSocketHttpHeaders(), URI.create("ws://"+host+":2376/v1.40/containers/"+containerName+"/attach/ws?stream=1&stdin=1&stdout=1&stderr=1")).get();
    }

    /**
     * 학습 컨테이너 attach
     * @param containerName
     * @param project
     */
    public void attachTrainingContainer(String containerName, ProjectSnapshot project, String projectType) throws ExecutionException, InterruptedException {
        URI uri = URI.create("ws://"+host+":2376/v1.40/containers/"+containerName+"/attach/ws?logs=1&stream=1&stdin=1&stdout=1&stderr=1");
        if("STT".equals(projectType)){
            // --- container log stream ---
            // runtime_error - file not found
            // Train.cpp:601 Shuffling trainset - 학습 전처리
            // Train.cpp:608 Epoch N started! - 학습 시작
            // Train.cpp:748 Finished training - 학습완료 (epoch 전체 완료)
            // Train.cpp:389 epoch (N회차 완료)

            // --- docker sdk status check ---
            // status 137 - docker stop
            // status 1 - keyboard interrupt
            // status 0 - 정상종료, 에러
            attachSttTrainingContainer(containerName, project, uri);
        }else if("LM".equals(projectType)){
            attachLmTrainingContainer(containerName, project, uri);
        }
    }

    /**
     * STT CONTAINER ATTACH
     * @param containerName
     * @param project
     * @param uri
     * @throws ExecutionException
     * @throws InterruptedException
     */
    private void attachSttTrainingContainer(String containerName, ProjectSnapshot project, URI uri)throws ExecutionException, InterruptedException {
        long before = System.currentTimeMillis();
        List<String> messages = new ArrayList<>();
        new StandardWebSocketClient().doHandshake(new TextWebSocketHandler() {

            @Override
            public void afterConnectionEstablished(WebSocketSession session) {
                log.info("DOCKER CLIENT - CONTAINER NAME {} CONNECTION", containerName);
            }

            @SneakyThrows
            @Override
            public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
                log.info("DOCKER CLIENT - CONTAINER NAME {} CLOSE SESSION", containerName);
                switch (dockerService.getContainerStateExitCode(containerName).intValue()){
                    case 137:
                        if(messages.stream().filter(v -> v.contains("Train.cpp:389")).count() == 0){
                            projectSnapshotRepository.updProjectSnapshotStatus(entityManager, project.getId(), ProjectStatus.STOP);
                        }else{
                            endTraining(project, "STT");
                        }
                        break;
                    case 0:
                        if(messages.stream().filter(v -> v.contains("runtime_error")).count() > 0){
                            projectSnapshotRepository.updProjectSnapshotStatus(entityManager, project.getId(), ProjectStatus.ERROR);
                        }
                        break;
                }
                String key = CONTAINER_NAME_PREFIX+project.getProjectId()+"_"+project.getId();
                RUNNING_TASK.remove(key);
                session.close();
            }

            @SneakyThrows
            @Override
            protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
                String msg = new String(message.getPayload().array(), StandardCharsets.UTF_8);
                messages.add(msg);
                log.info("MESSAGE : {}", msg);
                if(msg.contains("Train.cpp:389")){
                    long afterTime = System.currentTimeMillis();
                    projectSnapshotRepository.updProjectSnapshotStatusAndTimePerSession(entityManager, project.getId(), ProjectStatus.REPEAT, String.valueOf((((afterTime - before) / 1000)/60)));
                    EpochLogResponse epochLogResponse = new EpochLogResponse();
                    EpochLogResponse.Point loss = new EpochLogResponse.Point("loss");
                    EpochLogResponse.Point wer = new EpochLogResponse.Point("WER");
                    EpochLogResponse.Point ter = new EpochLogResponse.Point("TER");
                    Arrays.stream(msg.substring(msg.indexOf("]") + 1)
                                    .replaceAll("\\(.*?\\)| ", "")
                                    .replaceAll("-", "_").split("\\|"))
                            .filter(v -> {
                                String k = v.split(":")[0];
                                return k.contains("epoch") || k.contains("dev") || k.contains("loss") || k.contains("train");
                            })
                            .collect(Collectors.toMap(v -> v.split(":")[0], v -> v.split(":")[1]))
                            .forEach((key, value) -> {
                                if (key.contains("epoch")) {
                                    epochLogResponse.setEpoch(Integer.parseInt(value));
                                } else if (key.contains("dev")) {
                                    if (key.contains("TER")) {
                                        ter.setDev(Double.parseDouble(value));
                                    } else if (key.contains("WER")) {
                                        wer.setDev(Double.parseDouble(value));
                                    } else if (key.contains("loss")) {
                                        loss.setDev(Double.parseDouble(value));
                                    }
                                } else if (key.contains("train")) {
                                    if (key.contains("TER")) {
                                        ter.setTrain(Double.parseDouble(value));
                                    } else if (key.contains("WER")) {
                                        wer.setTrain(Double.parseDouble(value));
                                    }
                                } else if (key.contains("loss")) {
                                    loss.setTrain(Double.parseDouble(value));
                                }
                            });
                    epochLogResponse.setData(Arrays.asList(loss, wer, ter));
                    BigDecimal trainWer = BigDecimal.valueOf(wer.getTrain());
                    BigDecimal devWer = BigDecimal.valueOf(wer.getDev());
                    BigDecimal trainTer = BigDecimal.valueOf(ter.getTrain());
                    BigDecimal devTer = BigDecimal.valueOf(ter.getDev());
                    graphDataSnapshotRepository.save(
                            GraphDataSnapshot.builder()
                                    .epoch(epochLogResponse.getEpoch())
                                    .projectSnapshotId(project.getId())
                                    .bestTERScore(trainTer.compareTo(devTer) > 0 ? trainTer : devTer)
                                    .bestWERScore(trainWer.compareTo(devWer) > 0 ? trainWer : devWer)
                                    .dataPoints(new ObjectMapper().writeValueAsString(epochLogResponse))
                                    .build()
                    );
                }else if(msg.contains("Train.cpp:748")){
                    endTraining(project, "STT");
                }
            }

        }, new WebSocketHttpHeaders(), uri).get();
    }

    /**
     * LM CONTAINER ATTACH
     * @param containerName
     * @param project
     * @param uri
     * @throws ExecutionException
     * @throws InterruptedException
     */
    private void attachLmTrainingContainer(String containerName, ProjectSnapshot project, URI uri)throws ExecutionException, InterruptedException {
        List<String> messages = new ArrayList<>();
        new StandardWebSocketClient().doHandshake(new TextWebSocketHandler() {

            @Override
            public void afterConnectionEstablished(WebSocketSession session) {
                log.info("DOCKER CLIENT - CONTAINER NAME {} CONNECTION", containerName);
            }

            @SneakyThrows
            @Override
            public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
                log.info("DOCKER CLIENT - CONTAINER NAME {} CLOSE SESSION", containerName);
                switch (dockerService.getContainerStateExitCode(containerName).intValue()){
                    case 137:
                        if(messages.stream().filter(v -> v.contains("Done !")).count() == 0){
                            projectSnapshotRepository.updProjectSnapshotStatus(entityManager, project.getId(), ProjectStatus.STOP);
                        }else{
                            endTraining(project, "STT");
                        }
                        break;
                    case 0:
                        if(messages.stream().filter(v -> v.contains("ERROR")).count() > 0){
                            projectSnapshotRepository.updProjectSnapshotStatus(entityManager, project.getId(), ProjectStatus.ERROR);
                        }
                        break;
                }
                String key = CONTAINER_NAME_PREFIX+project.getProjectId()+"_"+project.getId();
                RUNNING_TASK.remove(key);
                session.close();
            }

            @SneakyThrows
            @Override
            protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
                String msg = new String(message.getPayload().array(), StandardCharsets.UTF_8);
                messages.add(msg);
                log.info("MESSAGE : {}", msg);
                template.convertAndSend("/ws/subscribe/training/lm/"+project.getProjectId()+"/"+project.getId(), msg);
                lmTrainingSnapshotRepository.save(
                        LmTrainingSnapshot.builder()
                                .projectSnapshot(project)
                                .logData(msg)
                                .build()
                );
                if(msg.contains("complete")){
                    endTraining(project, "LM");
                    session.close();
                }
            }

        }, new WebSocketHttpHeaders(), uri).get();
    }

    /**
     * 학습 종료처리
     * @param project 프로젝트 스냅샷
     * @throws JsonProcessingException
     */
    private void endTraining(ProjectSnapshot project, String projectType) throws JsonProcessingException {
        if("STT".equals(projectType)){
            List<GraphDataSnapshot.DataPoint> dataPoints = graphDataSnapshotRepository.findByProjectSnapshotIdOrderByRegDtAsc(project.getId());
            List<EpochLogResponse> list = dataPoints.parallelStream().map(v -> {
                try {
                    return new ObjectMapper().readValue(v.getDataPoints(), EpochLogResponse.class);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                    return new EpochLogResponse();
                }
            }).sorted(Comparator.comparing(EpochLogResponse::getEpoch)).collect(Collectors.toList());
            int finalEpoch = list.stream().max(Comparator.comparing(EpochLogResponse::getEpoch))
                    .get()
                    .getEpoch();
            graphDataRepository.save(
                    GraphData.builder()
                            .projectSnapshotId(project.getId())
                            .dataPoints(new ObjectMapper().writeValueAsString(list))
                            .finalEpoch(finalEpoch)
                            .build()
            );
            setModelTemplate(project, projectType);
        }else{

            setModelTemplate(project, projectType);
        }
        projectSnapshotRepository.updProjectSnapshotStatus(entityManager, project.getId(), ProjectStatus.END);
    }

    /**
     * 학습모델 저장
     * @param projectId 프로젝트 ID
     */
    @Transactional
    void setModelTemplate(ProjectSnapshot projectSnapshot, String projecType){
        Optional<Project> opt = projectRepository.findById(projectSnapshot.getProjectId());
        if(opt.isPresent()){
            Project project = opt.get();
            if("STT".equals(projecType)){
                ProjectStt stt = (ProjectStt)opt.get();
                modelTemplateRepository.save(
                        ModelTemplate.builder()
                                .projectSeq(projectSnapshot.getProjectId())
                                .snapshotSeq(projectSnapshot.getId())
                                .modelName(project.getModelName()+"-"+projectSnapshot.getName())
                                .modelPath("/DATA/AutoML_DATA/"+projectSnapshot.getProjectId()+"/"+projectSnapshot.getId()+"/result/model")
                                .modelFileName("001_model_dev-ATCH.bin")
                                .lastModelFileName("001_model_last.bin")
                                .engnType("EN_"+project.getProjectType()) // FIX prefix는 EN_으로 확정인지 확인 필요
                                .langCd(stt.getLanguage())
                                .smplRate(stt.getSampleRate())
                                .useEosYn(String.valueOf(stt.getEos()))
                                .userSeq(project.getUser().getId())
                                .build()
                );
            }else if("LM".equals(projecType)){
                ProjectLm lm = (ProjectLm)opt.get();
                modelTemplateRepository.save(
                        ModelTemplate.builder()
                                .projectSeq(projectSnapshot.getProjectId())
                                .snapshotSeq(projectSnapshot.getId())
                                .modelName(project.getModelName()+"-"+projectSnapshot.getName())
                                .modelPath("/DATA/AutoML_DATA/"+projectSnapshot.getProjectId()+"/"+projectSnapshot.getId()+"/result")
                                .modelFileName("lm.arpa.bin")
                                .lastModelFileName("lm.arpa.bin")
                                .engnType(project.getProjectType()) // FIX prefix는 EN_으로 확정인지 확인 필요
                                .langCd(lm.getLanguage())
                                .useEosYn(String.valueOf(lm.getEos()))
                                .userSeq(project.getUser().getId())
                                .build()
                );
            }
        }else{
            throw new EmptyResultDataAccessException(0);
        }
    }

}
