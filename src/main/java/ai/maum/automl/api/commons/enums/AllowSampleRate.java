package ai.maum.automl.api.commons.enums;

/**
 * ALLOW SAMPLE-RATE
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-23  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-23
 */
public enum AllowSampleRate {

    SR002(16000),
    SR001(8000);

    private final int value;

    AllowSampleRate(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static AllowSampleRate findByValue(int value){
        for(AllowSampleRate v : values()){
            if( v.value == value){
                return v;
            }
        }
        return null;
    }

}
