package ai.maum.automl.api.commons.enums;

/**
 * PROJECT STATUS
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-07-30
 */
public enum ProjectStatus {

    CREATE,
    WAIT,
    START,
    REPEAT,
    END,
    STOP,
    ERROR,

}
