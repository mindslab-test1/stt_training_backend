package ai.maum.automl.api.commons.exception;

/**
 * 허용되지 않은 파일에 대한 예외
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-23  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-23
 */
public class FileNotAllowedException extends RuntimeException {

    public FileNotAllowedException(String msg) {
        super(msg);
    }

}
