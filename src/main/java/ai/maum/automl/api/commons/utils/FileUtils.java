package ai.maum.automl.api.commons.utils;

import ai.maum.automl.api.commons.enums.AllowFileExt;
import ai.maum.automl.api.commons.enums.AllowSampleRate;
import ai.maum.automl.api.commons.exception.FileNotAllowedException;
import ai.maum.automl.api.config.SFTPConfig;
import ai.maum.automl.api.model.entity.automl.project.LearningData;
import ai.maum.automl.api.model.entity.automl.project.UploadLearningData;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Files;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.jcraft.jsch.SftpProgressMonitor;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.TagException;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * ai.maum.automl.api.commons.utils
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-10-08
 */
@Log4j2
@Component
@RequiredArgsConstructor
public class FileUtils {

    private final SFTPConfig sftpConfig;
    private final Environment environment;

    /**
     * 오디오 파일 생성
     * @param id 프로젝트 ID
     * @param v 파일
     * @return 오디오 파일 정보
     * @throws IOException
     * @throws CannotReadException
     * @throws TagException
     * @throws InvalidAudioFrameException
     * @throws ReadOnlyFileException
     */
    public Map<String, Object> createAudioFile(String uuid, Long id, MultipartFile v, String rootPath, String trainPath) throws IOException, CannotReadException, TagException, InvalidAudioFrameException, ReadOnlyFileException {
        File file = createFile(uuid, id, v, rootPath, trainPath);
        AudioFile audioFile = AudioFileIO.read(file);
        if(!Optional.ofNullable(AllowSampleRate.findByValue(audioFile.getAudioHeader().getSampleRateAsNumber())).isPresent()){
            file.delete();
            throw new FileNotAllowedException("not allow file - sample-rate mismatch");
        }
        return ImmutableMap.of("audio", audioFile.getAudioHeader(), "file", file, "multipart", v);
    }

    /**
     * 텍스트 파일 생성
     * @param id 프로젝트 ID
     * @param v 파일
     * @param lang 프로젝트 설정 언어
     * @return 텍스트 파일 정보
     * @throws IOException
     * @throws FileNotAllowedException
     */
    public Map<String, Object> createTxtFile(String uuid, Long id, MultipartFile v, String lang, String rootPath, String trainPath) throws FileNotAllowedException, IOException {
        File file = createFile(uuid, id, v, rootPath, trainPath);
        if(Files.readLines(file, StandardCharsets.UTF_8)
                .parallelStream()
                /*.map(sv -> "LN_EN".equals(lang) ? sv.replaceAll("[^a-zA-Z\\$\\s]*", "") : sv.replaceAll("[^가-힣\\s\\$]*", ""))*/ // replace를 왜 하지..?
                .allMatch(sv -> {
                    boolean rtn = "LN_EN".equals(lang) ? Pattern.matches("[a-zA-Z\\s\\$]*$", sv) : Pattern.matches("[ㄱ-ㅎ가-힣\\s\\$]*$", sv);
                    return rtn;
                })){
            return ImmutableMap.of("file", file, "multipart", v);
        }else{
            file.delete();
            throw new FileNotAllowedException("not allow file - txt file content language mismatch");
        }
    }

    /**
     * 파일 삭제
     * @param list 학습데이터 목록
     */
    public void deleteFile(List<UploadLearningData> list){
        if(Arrays.stream(environment.getActiveProfiles()).anyMatch(sv -> sv.equalsIgnoreCase("local"))){
            ChannelSftp sftp = null;
            Session session = null;
            try {
                session = (Session) sftpConfig.connectSFTP().get("session");
                sftp = (ChannelSftp) sftpConfig.connectSFTP().get("channel");
                ChannelSftp finalSftp = sftp;
                list.forEach(v -> {
                    try {
                        finalSftp.rm(v.getAtchFilePath());
                    } catch (SftpException e) {
                        throw Throwables.propagate(e);
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                sftp.disconnect();
                session.disconnect();
            }
        }else{
            list.forEach(v -> new File(v.getAtchFilePath()).delete());
        }
    }

    /**
     * CREATE FILE
     * @param id 프로젝트 ID
     * @param v 파일
     * @return
     * @throws IOException
     */
    private File createFile(String uuid, Long id, MultipartFile v, String rootPath, String trainPath) throws IOException {
        final String projectPath = rootPath+"/"+id;
        final String path = rootPath+"/"+id+trainPath;
        Path temp = java.nio.file.Files.createTempFile(null, AllowFileExt.findExt(v.getOriginalFilename()));
        if(Arrays.stream(environment.getActiveProfiles()).anyMatch(sv -> sv.equalsIgnoreCase("local"))){
            ChannelSftp sftp = null;
            Session session = null;
            try {
                session = (Session) sftpConfig.connectSFTP().get("session");
                sftp = (ChannelSftp) sftpConfig.connectSFTP().get("channel");
                ChannelSftp finalSftp = sftp;
                InputStream inputStream;
                try {
                    inputStream = v.getInputStream();
                    String fileName = "/" + uuid + AllowFileExt.findExt(v.getOriginalFilename());
                    try {
                        finalSftp.mkdir(projectPath);
                    } catch (SftpException e) {
                        // ignore..
                    }
                    try {
                        finalSftp.mkdir(path);
                    } catch (SftpException e) {
                        // ignore..
                    }
                    finalSftp.put(inputStream, path+fileName, new SftpProgressMonitor() {

                        private long max = 0;
                        private long count = 0;
                        private long percent = 0;

                        @Override
                        public void init(int op, String src, String dest, long max) {
                            this.max = max;
                        }

                        @Override
                        public void end() {

                        }

                        @Override
                        public boolean count(long bytes) {
                            this.count += bytes;
                            long percentNow = this.count*100/max;
                            if(percentNow>this.percent){
                                this.percent = percentNow;
                                log.info("progress : {}%", this.percent);
                            }
                            return true;
                        }

                    });
                    org.apache.commons.io.FileUtils.copyInputStreamToFile(sftp.get(path+"/"+ uuid + AllowFileExt.findExt(v.getOriginalFilename())), temp.toFile());
                    File renameFile = new File(temp.toFile().getAbsolutePath().replaceAll(temp.toFile().getName(), "")+uuid + AllowFileExt.findExt(v.getOriginalFilename()));
                    temp.toFile().renameTo(renameFile);
                    temp = Path.of(renameFile.getPath());
                } catch (IOException | SftpException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                sftp.disconnect();
                session.disconnect();
                return temp.toFile();
            }
        }else{
            File file = new File(rootPath+trainPath+"/"+id+"/"+ uuid + AllowFileExt.findExt(v.getOriginalFilename()));
            org.apache.commons.io.FileUtils.touch(file);
            v.transferTo(file);
            return file;
        }
    }

}
