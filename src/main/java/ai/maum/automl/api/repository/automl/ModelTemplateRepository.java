package ai.maum.automl.api.repository.automl;

import ai.maum.automl.api.model.entity.automl.project.ModelTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

/**
 * ModelTemplateRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-28  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-28
 */
@Repository
public interface ModelTemplateRepository extends JpaRepository<ModelTemplate, Long> {

    /**
     * 엔진타입, 언어 코드, 샘플링레이트 코드와 유저 SEQ로 모델 템플릿 조회
     * @param id 회원 ID (고유번호), NULL일 경우 사전학습모델
     * @param engnType 엔진 타입
     * @param langCd 언어 코드
     * @param smplRate 샘플링레이트 코드
     * @return 모델 템플릿 목록
     */
    List<ModelTemplate> findByUserSeqEqualsAndEngnTypeEqualsAndLangCdEqualsAndSmplRateEquals(Long id, String engnType, String langCd, String smplRate);

    /**
     * 엔진타입, 언어 코드로 사전 학습모델 조회
     * @param engnType 엔진 타입
     * @param langCd 언어 코드
     * @return 모델 템플릿 목록
     */
    List<ModelTemplate> findByUserSeqIsNullAndEngnTypeEqualsAndLangCdEquals(String engnType, String langCd);

    /**
     * 유저 SEQ와 엔진타입, 언어코드로 모델 템플릿 조회 (LM조회)
     * @param id 회원 ID (고유번호), NULL일 경우 사전학습모델
     * @param engnType 엔진 타입
     * @param langCd 언어 코드
     * @return 모델 템플릿 목록
     */
    List<ModelTemplate> findByUserSeqEqualsAndEngnTypeEqualsAndLangCdEquals(Long id, String engnType, String langCd);

    /**
     * 프로젝트Seq, snapshotSeq로 모델 조회
     * @param projectSeq 프로젝트 ID
     * @param snapshotSeq 스냅샷 ID
     * @return 모델 템플릿
     */
    ModelTemplate findByProjectSeqEqualsAndSnapshotSeqEquals(Long projectSeq, Long snapshotSeq);

    /**
     * 회원SEQ, 프로젝트Seq, 엔진타입, 언어 코드, 샘플링레이트로 모델 스트림 조회
     * @param userSeq 회원 ID
     * @param projectSeq 프로젝트 ID
     * @param engnType 엔진 타입
     * @param langCd 언어 코드
     * @param smplRate 샘플링레이트 코드
     * @return 모델 템플릿 stream
     */
    List<ModelTemplate> findByUserSeqEqualsAndProjectSeqEqualsAndEngnTypeEqualsAndLangCdEqualsAndSmplRateEquals(Long userSeq, Long projectSeq, String engnType, String langCd, String smplRate);

    /**
     * 회원SEQ, 프로젝트Seq, 엔진타입, 언어 코드로 모델 스트림 조회
     * @param userSeq 회원 ID
     * @param projectSeq 프로젝트 ID
     * @param engnType 엔진 타입
     * @param langCd 언어 코드
     * @return 모델 템플릿 stream
     */
    List<ModelTemplate> findByUserSeqEqualsAndProjectSeqEqualsAndEngnTypeEqualsAndLangCdEquals(Long userSeq, Long projectSeq, String engnType, String langCd);

}
