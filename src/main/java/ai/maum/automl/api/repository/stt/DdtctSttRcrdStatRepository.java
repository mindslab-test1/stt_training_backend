package ai.maum.automl.api.repository.stt;

import ai.maum.automl.api.model.entity.stt.DdtctSttRcrdStat;
import ai.maum.automl.api.model.entity.stt.DdtctSttRcrdStatId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * DdtctSttRcrdStatRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-22  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-22
 */
@Repository
public interface DdtctSttRcrdStatRepository extends JpaRepository<DdtctSttRcrdStat, DdtctSttRcrdStatId> {

    /**
     * 데이터 처리 모니터링 현황
     * @param startDate 조회 시작일자
     * @param endDate 조회 종료일자
     * @return
     */
    List<DdtctSttRcrdStat> findByDntRgstGreaterThanEqualAndDntRgstLessThanEqualOrderByDntRgstAsc(LocalDateTime startDate, LocalDateTime endDate);

}