package ai.maum.automl.api.repository.automl;

import ai.maum.automl.api.model.entity.automl.project.GraphData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-24
 */
@Repository
public interface GraphDataRepository extends CrudRepository<GraphData, Long> {
    GraphData findByProjectSnapshotId(Long projectSnapshotId);
}
