package ai.maum.automl.api.repository.automl;

import ai.maum.automl.api.model.entity.automl.project.Queue;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by yejoon3117@mindslab.ai on 2020-11-24
 */
@Repository
public interface QueueRepository extends CrudRepository<Queue, Long> {
    @Query("SELECT count (q2) FROM Queue as q2 WHERE q2.regDt < " +
            "(SELECT q.regDt FROM Queue as q WHERE q.snapshotId = :snapshotId)")
    int findOrder(@Param("snapshotId") Long snapshotId);
}