package ai.maum.automl.api.repository.automl;

import ai.maum.automl.api.model.entity.automl.project.LmTrainingSnapshot;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * ai.maum.automl.api.repository.automl
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 * </pre>
 * @since 2021-10-15
 */
public interface LmTrainingSnapshotRepository extends CrudRepository<LmTrainingSnapshot, Long> {

    Optional<List<LmTrainingSnapshot>> findLmTrainingSnapshotByProjectSnapshotIdEquals(Long projectSnapshotId);

}
