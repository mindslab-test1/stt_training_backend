package ai.maum.automl.api.repository.automl;

import ai.maum.automl.api.model.entity.automl.common.User;
import ai.maum.automl.api.model.entity.automl.project.Project;
import ai.maum.automl.api.model.entity.automl.project.ProjectExInterface;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by kirk@mindslab.ai on 2020-10-22
 */
@Repository
public interface ProjectRepository extends PagingAndSortingRepository<Project, Long> {
    Page<Project> findAllByUserIdOrderByIdDesc(Long userId, Pageable pageable);

    Optional<Project> findByIdEqualsAndUserEquals(Long projectId, User user);

    List<Project> findByUserIdAndModelName(Long userId, String modelName);
    @Query(value =
            "select " +
            "  p.id as id, p.project_name as projectName, p.project_description as projectDescription," +
            "  p.project_type as projectType, p.model_name as modelName, " +
            "  p.reg_dt as regDt, p.mod_dt as modDt, " +
            "  count(ps.id) as modelCnt, " +
            "  (select status from project_snapshot s where s.project_id = p.id and (pstt.completed_step > 1 or ptts.completed_step > 1 or plm.completed_step > 1) order by s.id desc limit 1) as status " +
            "from " +
            "  project p " +
            "  left outer join project_stt pstt on pstt.id = p.id " +
            "  left outer join project_lm plm on plm.id = p.id " +
            "  left outer join project_tts ptts on ptts.id = p.id " +
            "  left outer join project_snapshot ps on p.id = ps.project_id and ps.status = 'END' " +
            "where " +
            "  p.user_id = :userId " +
            "group by p.id " +
            "order by p.id desc"
            , countQuery =
            "select " +
            "  count(*) " +
            "from " +
            "  project p " +
            "where " +
            "  p.user_id = :userId "
            , nativeQuery = true)
    Page<ProjectExInterface> findAllByUserId(@Param("userId") Long userId, Pageable pageable);

}
