package ai.maum.automl.api.repository.automl;

import ai.maum.automl.api.model.entity.automl.project.LearningData;
import ai.maum.automl.api.model.entity.automl.project.QUploadLearningData;
import ai.maum.automl.api.model.entity.automl.project.UploadLearningData;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * LearningDataRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-23  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-23
 */
@Repository
public interface LearningDataRepository extends JpaRepository<LearningData, Long> {

    /**
     * atchFileId로 데이터 삭제
     * @param atchFileId
     */
    void deleteLearningDataByAtchFileIdEquals(String atchFileId);

}
