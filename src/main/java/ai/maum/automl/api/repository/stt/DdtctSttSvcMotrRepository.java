package ai.maum.automl.api.repository.stt;

import ai.maum.automl.api.model.dto.monitoring.ServiceMonitoringDto;
import ai.maum.automl.api.model.entity.stt.DdtctSttSvcMotr;
import ai.maum.automl.api.model.entity.stt.DdtctSttSvcMotrId;
import ai.maum.automl.api.model.entity.stt.QDdtctCodeDtlTb;
import ai.maum.automl.api.model.entity.stt.QDdtctSttSvcMotr;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * DdtctSttSvcMotrRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-06-22  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-06-22
 */
@Repository
public interface DdtctSttSvcMotrRepository extends JpaRepository<DdtctSttSvcMotr, DdtctSttSvcMotrId> {

    QDdtctSttSvcMotr ddtctSttSvcMotr = QDdtctSttSvcMotr.ddtctSttSvcMotr;
    QDdtctCodeDtlTb svcDdtctCodeDtlTb = new QDdtctCodeDtlTb("svc");
    QDdtctCodeDtlTb servIdDdtctCodeDtlTb = new QDdtctCodeDtlTb("serv");

    /**
     * 서비스 모니터링 목록 조회 (공통코드 join)
     * @param entityManager
     * @return
     */
    default List<ServiceMonitoringDto> findDdtctSttSvcMotrs(EntityManager entityManager){
        return new JPAQuery<ServiceMonitoringDto>(entityManager)
                .select(
                        Projections.bean(
                                ServiceMonitoringDto.class,
                                ddtctSttSvcMotr.servId,
                                ddtctSttSvcMotr.svcId,
                                ddtctSttSvcMotr.svcStatCd,
                                ddtctSttSvcMotr.oprtDate,
                                ddtctSttSvcMotr.svcStDate,
                                ddtctSttSvcMotr.svcEdDate,
                                ddtctSttSvcMotr.updatedTm,
                                svcDdtctCodeDtlTb.cdNm.as("svcIdNm"),
                                servIdDdtctCodeDtlTb.cdNm.as("servIdNm")
                        )
                ).from(ddtctSttSvcMotr)
                .innerJoin(svcDdtctCodeDtlTb).on(svcDdtctCodeDtlTb.cd.eq(ddtctSttSvcMotr.svcId).and(svcDdtctCodeDtlTb.cdGp.eq("SVC_ID")))
                .innerJoin(servIdDdtctCodeDtlTb).on(servIdDdtctCodeDtlTb.cd.eq(ddtctSttSvcMotr.servId).and(servIdDdtctCodeDtlTb.cdGp.eq("SERV_ID")))
                .fetch();
    }

}