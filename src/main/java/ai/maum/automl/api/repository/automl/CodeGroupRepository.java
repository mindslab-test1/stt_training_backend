package ai.maum.automl.api.repository.automl;

import ai.maum.automl.api.model.entity.automl.common.CodeGroup;
import ai.maum.automl.api.model.entity.automl.common.QCode;
import ai.maum.automl.api.model.entity.automl.common.QCodeGroup;
import com.querydsl.jpa.impl.JPAQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

/**
 * ai.maum.automl.frontapi.repository.automl
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-21  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-21
 */
@Repository
public interface CodeGroupRepository extends JpaRepository<CodeGroup, String> {

    QCodeGroup codeGroup = QCodeGroup.codeGroup1;
    QCode code = QCode.code1;

    /**
     * 코드그룹 코드로 코드그룹 엔티티 및 코드목록 조회
     * @param entityManager em
     * @param cg 코드그룹
     * @return
     */
    default CodeGroup findCodeGroupByCodeGroupEqual(EntityManager entityManager, String cg){
        return new JPAQuery<>(entityManager)
                .select(
                        codeGroup
                ).from(codeGroup)
                .join(codeGroup.codes)
                .fetchJoin()
                .where(codeGroup.codeGroup.eq(cg))
                .fetchOne();
    }

}
