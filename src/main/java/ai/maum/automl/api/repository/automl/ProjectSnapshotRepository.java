package ai.maum.automl.api.repository.automl;

import ai.maum.automl.api.commons.enums.ProjectStatus;
import ai.maum.automl.api.model.dto.project.ProjectSnapshotDtoInterface;
import ai.maum.automl.api.model.entity.automl.project.ProjectSnapshot;
import ai.maum.automl.api.model.entity.automl.project.QProjectSnapshot;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * Created by kirk@mindslab.ai on 2020-10-22
 */
@Repository
public interface ProjectSnapshotRepository extends CrudRepository<ProjectSnapshot, Long> {

    QProjectSnapshot projectSnapshot = QProjectSnapshot.projectSnapshot;

//    List<ProjectSnapshot> findAllByProjectIdOrderByRegDtDesc(Long id);
    @Query(value = "" +
            "select " +
            " ps.id, ps.name, ps.snapshot, ps.reg_dt as regDt, ps.model_id as modelId, ps.status, " +
            " gd.bestTer, gd.bestTerEpoch, gd.bestWer, gd.bestWerEpoch " +
            "from " +
            " project_snapshot ps " +
            " left outer join graph_data gd on ps.id = gd.project_snapshot_id " +
            "where ps.project_id = :id " +
            "order by ps.reg_dt desc" , nativeQuery = true)
    List<ProjectSnapshotDtoInterface> findAllByProjectIdOrderByRegDtDesc(Long id);

    Optional<ProjectSnapshot> findTop1ByProjectIdOrderByRegDtDesc(Long projectId);

    List<ProjectSnapshot.ModelId> findByProjectIdAndStatus(Long projectId, String status);

    /**
     * 프로젝트 스테이터스 조건에 일치하는 스냅샷 목록 반환
     * @param statuses 스테이터스 목록
     * @return 스냅샷 목록
     */
    List<ProjectSnapshot> findByStatusIn(List<ProjectStatus> statuses);

    @Transactional
    void deleteProjectSnapshotById(Long id);

    /**
     * update snapshot status
     * @param entityManager em
     * @param id snapshot id
     * @param status project status
     * @return
     */
    @Transactional
    default boolean updProjectSnapshotStatus(EntityManager entityManager, Long id, ProjectStatus status){
        return new JPAUpdateClause(entityManager, projectSnapshot)
                .set(projectSnapshot.status, status)
                .where(projectSnapshot.id.eq(id))
                .execute() > 0;
    }


    /**
     * update snapshot status and time
     * @param entityManager em
     * @param id snapshot id
     * @param status project status
     * @return
     */
    @Transactional
    default boolean updProjectSnapshotStatusAndTimePerSession(EntityManager entityManager, Long id, ProjectStatus status, String time){
        return new JPAUpdateClause(entityManager, projectSnapshot)
                .set(projectSnapshot.status, status)
                .set(projectSnapshot.timePerSession, time+"분")
                .where(projectSnapshot.id.eq(id))
                .execute() > 0;
    }

}
