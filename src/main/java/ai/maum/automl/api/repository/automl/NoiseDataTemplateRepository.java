package ai.maum.automl.api.repository.automl;

import ai.maum.automl.api.model.entity.automl.project.NoiseDataTemplate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * NoiseDataTemplateRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-27  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-27
 */
@Repository
public interface NoiseDataTemplateRepository extends JpaRepository<NoiseDataTemplate, Long> {

    /**
     * 샘플링레이트로 노이즈 데이터 템플릿 리스트 반환
     * @param smplRate 샘플링레이트 code
     * @return
     */
    List<NoiseDataTemplate> findBySmplRateEquals(String smplRate);

}
