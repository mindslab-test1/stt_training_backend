package ai.maum.automl.api.repository.automl;

import ai.maum.automl.api.model.entity.automl.project.QUploadLearningData;
import ai.maum.automl.api.model.entity.automl.project.UploadLearningData;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.jpa.impl.JPADeleteClause;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAUpdateClause;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

/**
 * UploadLearningDataRepository
 *
 * @author MINDS
 * @version 1.0
 * @see <pre>
 *  Modification Information
 *
 * 	수정일     / 수정자   / 수정내용
 * 	------------------------------------------
 * 	2021-07-23  / 최재민	 / 최초 생성
 * </pre>
 * @since 2021-07-23
 */
@Repository
public interface UploadLearningDataRepository extends JpaRepository<UploadLearningData, Long> {

    QUploadLearningData uploadLearningData = QUploadLearningData.uploadLearningData;

    /**
     * 미사용 데이터 삭제
     * @param used
     * @return
     */
    List<UploadLearningData> findUploadLearningDataByIsUsedEquals(boolean used);

    /**
     * uploadSeq를 통해 pair조회 후 리스트 반환
     * @param uploadSeq
     * @return
     */
    default List<UploadLearningData> findUploadLearningDataById(EntityManager entityManager, Long uploadSeq){
        return new JPAQuery<UploadLearningData>(entityManager)
                .from(uploadLearningData)
                .where(
                        uploadLearningData.atchFileName.like("%"+
                                new JPAQuery<UploadLearningData>(entityManager)
                                        .select(uploadLearningData.atchFileName)
                                        .from(uploadLearningData)
                                        .where(uploadLearningData.uploadSeq.eq(uploadSeq))
                                        .fetchOne().split("\\.")[0]+"%"
                        )
                ).fetch();
    }

    /**
     * 프로젝트에 사용된 seq 사용처리
     * @param entityManager
     * @param uploadSeq
     * @return
     */
    default boolean updateIsUsedByUpdateSeqIn(EntityManager entityManager, List<Long> uploadSeq){
        return new JPAUpdateClause(entityManager, uploadLearningData)
                .set(uploadLearningData.isUsed, true)
                .where(uploadLearningData.uploadSeq.in(uploadSeq))
                .execute() > 0;
    }

    /**
     * 미사용 파일 삭제
     * @param isUsed
     */
    default boolean deleteByIsUsed(EntityManager entityManager, boolean isUsed){
        return new JPADeleteClause(entityManager, uploadLearningData)
                .where(uploadLearningData.isUsed.eq(isUsed))
                .execute() > 0;
    }

}
