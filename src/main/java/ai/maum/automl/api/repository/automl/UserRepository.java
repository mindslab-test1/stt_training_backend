package ai.maum.automl.api.repository.automl;

import ai.maum.automl.api.model.entity.automl.common.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by kirk@mindslab.ai on 2020-10-22
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findById(Long id);
    User findByEmail(String email);
    Optional<User> findAllById(Long id);
}
